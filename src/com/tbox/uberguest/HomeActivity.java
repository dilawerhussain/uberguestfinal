package com.tbox.uberguest;

import java.io.File;
import android.location.LocationManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.android.volley.toolbox.ImageLoader;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tbox.backend.AppController;
import com.tbox.backend.FileDownloader;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends Activity {
	TextView tv_username, tv_email;
//	SharedPreferences settings;
	static Context context;
	static boolean flagImageUpdate;
	ImageView iv_profile_image;
	Button btn_edit_profile, btn_logout, btn_map;
	static Uri selectedImage;
	String ime;
	
	public static LocationManager locationMangaer = null;
	Animation animation;
	// Boolean IsNotified = false;
	String cur_prop_name = "company";
	String cur_prop_lat = "0.0";
	String cur_prop_long = "0.0";

	private BroadcastReceiver mReceiver;

	private void openLoginActivityIfLogout() {
		UserInfo userInfo = UserInfo.getInstance(HomeActivity.this);
		// check if login
		if (userInfo.is_login == false) {
			Intent home_intent = new Intent(HomeActivity.this,
					LoginActivity.class);
			ScreenType.Home_Default.attachTo(home_intent);
			startActivity(home_intent);
			finish();
		}
	}

	public static void displayPromptForEnablingGPS(final Activity activity) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
		final String message = "GPS is disabled, please enable";
		builder.setMessage(message)
				.setPositiveButton("Settings",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface d, int id) {
								if (locationMangaer.getAllProviders().contains("gps")) 
								{
									Log.e("GPS", "Device GPS Enabled "+ locationMangaer.getAllProviders().contains("gps"));
									activity.startActivity(new Intent(action));
									d.dismiss();
								}
								else 
								{
									Log.e("GPS", "Device GPS Enabled "+ locationMangaer.getAllProviders().contains("gps"));
									Utility.showAlertDialog("Your device is not GPS enabled","Your device has not GPS", activity);

								}
								
							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface d, int id) {
								d.cancel();
							}
						});
		builder.create().show();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
//		unregisterReceiver(mReceiver);
		super.onStop();
	}
	
	
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		SharedPreferences sp = getSharedPreferences("isNotified", Activity.MODE_PRIVATE);
		Log.e("inBackground",""+ sp.getInt("inFocus", 0));
		SharedPreferences.Editor editor = sp.edit();
        editor.putInt("inFocus", 0);
        editor.commit();
		
//		unregisterReceiver(mReceiver);
	}
	
	private void registerAllElements() {
		btn_map = (Button) findViewById(R.id.btn_map);
		tv_username = (TextView) findViewById(R.id.userNameGuestHome);
		tv_email = (TextView) findViewById(R.id.userEmailGuestHome);
		iv_profile_image = (ImageView) findViewById(R.id.userImageHome);
		btn_logout = (Button) findViewById(R.id.logoutHome);
		btn_edit_profile = (Button) findViewById(R.id.editHome);
		// displayPromptForEnablingGPS(this);
	}

	private void attachListeners() {

		btn_map.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(),
						MapActivity.class);
				SharedPreferences sp = getSharedPreferences("isNotified",
						Activity.MODE_PRIVATE);
				int notified = sp.getInt("notified", 0);
				Log.e("NotifiedHome", "" + notified);
				if (notified == 1) {
					i.putExtra("property_name", sp.getString("propertyName", "tbox"));
					i.putExtra("longitude", sp.getString("cur_prop_long","0.0"));
					i.putExtra("latitude", sp.getString("cur_prop_lat","0.0"));
					startActivity(i);
					v.clearAnimation();
					btn_map.setBackgroundResource(R.drawable.blue1);
				} else{
					startActivity(i);
					
				}
			}
		});
		btn_logout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				new AlertDialog.Builder(HomeActivity.this)
						.setTitle("")
						.setMessage(R.string.logout_confirmation_text)
						.setPositiveButton(R.string.alert_yes,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {

										// check if there is internet connection
										// logout, else show internet connection
										// error message
										final ConnectivityManager conMgr = (ConnectivityManager) HomeActivity.this
												.getSystemService(Context.CONNECTIVITY_SERVICE);
										final NetworkInfo activeNetwork = conMgr
												.getActiveNetworkInfo();
										if (activeNetwork != null
												&& activeNetwork.isConnected()) {
											// send request to api, if there is
											// successful logout, remove
											// userinfo from the app
											RequestParams params = new RequestParams();
											UserInfo userInfo = UserInfo
													.getInstance(HomeActivity.this);
											params.put(Constants.kapi_key,
													userInfo.api_key);
											params.put("device_id", ime);

											// send post request for user login
											AsyncHttpClient client = new AsyncHttpClient();
											client.get(
													Constants.api_base_url
															+ Constants.guest_logout,
													params,
													new JsonHttpResponseHandler() {
														@SuppressLint({
																"CommitPrefEdits",
																"Wakelock" })
														@Override
														public void onSuccess(
																final JSONObject object) {
															int status = 0;
															try {
																status = object
																		.getInt("status");
															} catch (JSONException e) {
																// TODO
																// Auto-generated
																// catch block
																e.printStackTrace();
															}
															if (status == 1) {
																UserInfo userInfo = UserInfo
																		.getInstance(HomeActivity.this);
																userInfo.removeUserInfo(HomeActivity.this);
																HomeActivity.this
																		.stopService(new Intent(
																				HomeActivity.this,
																				ServiceUberGuest.class));
																Intent intent_logout = new Intent(
																		HomeActivity.this,
																		LoginActivity.class);
																startActivity(intent_logout);
																Utility.profile_image_path="";
																finish();
															}
														}

														public void onFailure(
																Throwable e,
																JSONObject errorResponse) {
															openLoginActivityIfLogout();
															int status = 0;
															try {
																status = errorResponse
																		.getInt("status");
															} catch (JSONException e1) {
																// TODO
																// Auto-generated
																// catch block
																e1.printStackTrace();
															}
															if (status == -2) {
																UserInfo userInfo = UserInfo
																		.getInstance(HomeActivity.this);
																userInfo.removeUserInfo(HomeActivity.this);
																HomeActivity.this
																		.stopService(new Intent(
																				HomeActivity.this,
																				ServiceUberGuest.class));
																Intent intent_logout = new Intent(
																		HomeActivity.this,
																		LoginActivity.class);
																startActivity(intent_logout);
																finish();
															}
															Log.d("testing",
																	errorResponse
																			.toString());

														}

														public void onFailure(
																Throwable e,
																JSONArray errorResponse) {
															Log.d("api",
																	"api failure");
														}
													});
										} else {
											Toast.makeText(HomeActivity.this,
													"network is not working",
													Toast.LENGTH_LONG).show();
										}
									}
								})
						.setNegativeButton(R.string.alert_no,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										// do nothing
									}
								}).show();
			}
		});

		btn_edit_profile.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// prompt dialog
				AlertDialog.Builder alert = new AlertDialog.Builder(
						HomeActivity.this);

				alert.setTitle("");
				alert.setMessage("Confirm your password");

				// Set an EditText view to get user input
				final EditText input = new EditText(HomeActivity.this);
				input.setTransformationMethod(PasswordTransformationMethod
						.getInstance());
				alert.setView(input);

				alert.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								UserInfo userInfo = UserInfo
										.getInstance(HomeActivity.this);
								String value = input.getText().toString();
								if (value.equals(userInfo.password)) {
									Intent editProfile = new Intent(
											HomeActivity.this,
											EditProfileActivity.class);
									startActivity(editProfile);
								} else {
									Toast.makeText(HomeActivity.this,
											"Please enter correct password",
											Toast.LENGTH_LONG).show();
								}
							}
						});

				alert.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {

								// Canceled.
							}
						});

				alert.show();
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
			}
		});

	}

	@SuppressLint("SdCardPath")
	private void setPrefillValues() {
		UserInfo userInfo = UserInfo.getInstance(HomeActivity.this);
		ImageLoader imageLoader = AppController.getInstance().getImageLoader();
        imageLoader.get(userInfo.profile_image_path, ImageLoader.getImageListener(
        		iv_profile_image, R.drawable.profile_placeholder, R.drawable.profile_placeholder));
        
        Log.e("profile image path", userInfo.profile_image_path+"");
        
		Intent intent = getIntent();
		ScreenType screenTypeObj = ScreenType.detachFrom(intent);
		if (screenTypeObj == ScreenType.Home_From_Login) {
			File f = new File(Utility.getString(HomeActivity.this, R.string.voice_defalut_path));
			if (f.exists()) {
				f.delete();
			}
			FileDownloader fileDownloader = new FileDownloader();
			fileDownloader.fileName = "voice.wav";
			fileDownloader.filePath = "UberGuest";
			fileDownloader.execute(userInfo.name_recording_path);
		}
		tv_email.setText(userInfo.email_id);
		tv_username.setText(userInfo.first_name + " " + userInfo.last_name);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.home_activity);
		
		locationMangaer = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
		registerAllElements();
		attachListeners();
		// flagImageUpdate = false;
		// context = this;
		animation = new AlphaAnimation(1, 0); // Change alpha from fully visible
												// to invisible
		animation.setDuration(500); // duration - half a second
		animation.setInterpolator(new LinearInterpolator()); // do not alter
																// animation
																// rate
		animation.setRepeatCount(Animation.INFINITE); // Repeat animation
														// infinitely
		animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the
													// end so the button will
													// fade back in

		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(this.TELEPHONY_SERVICE);
		ime = telephonyManager.getDeviceId();
		Log.e("Unique ID", "IME" + telephonyManager.getDeviceId());
		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		// Log.e("GPS", "Status " +
		// manager.isProviderEnabled(LocationManager.GPS_PROVIDER));
		Log.e("GPS","Gps status "+ manager.isProviderEnabled(LocationManager.GPS_PROVIDER));
		if (!statusOfGPS)
		{
			displayPromptForEnablingGPS(this);
		} 
		else 
		{
			Log.e("GPS","Logged In "+ manager.isProviderEnabled(LocationManager.GPS_PROVIDER));
		}
		if (isNetworkAvailable() && !isMyServiceRunning()) 
		{
			Intent mService = new Intent(this, ServiceUberGuest.class);
			startService(mService);
		}
		
		UserInfo userInfo = UserInfo.getInstance(HomeActivity.this);
		if(userInfo.is_password_changed.equals("Yes"))
		{
			dialogueToChanjePassword();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		setPrefillValues();
		// TODO Auto-generated method stub
		SharedPreferences sp = getSharedPreferences("isNotified", Activity.MODE_PRIVATE);
		Log.e("inBackground",""+ sp.getInt("inFocus", 2));
		SharedPreferences.Editor editor = sp.edit();
        editor.putInt("inFocus", 2);
        editor.commit();
	    int notified = sp.getInt("notified", 0);
		IntentFilter intentFilter = new IntentFilter("android.intent.action.MAIN");
		mReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				// extract our message from intent
				cur_prop_name = intent.getStringExtra("property_name");
				cur_prop_long = intent.getStringExtra("longitude");
				cur_prop_lat = intent.getStringExtra("latitude");
				// log our message value
				btn_map.setBackgroundResource(R.drawable.red1);
				btn_map.startAnimation(animation);
				Log.i("Notified", cur_prop_name);
			}
		};
		// registering our receiver
		this.registerReceiver(mReceiver, intentFilter);
//		if(!Utility.profile_image_path.isEmpty()){
//			Bitmap bitmap = BitmapFactory.decodeFile(Utility.profile_image_path);
//			iv_profile_image.setImageBitmap(bitmap);
//		}
		
		if(notified==0){
			btn_map.clearAnimation();
			btn_map.setBackgroundResource(R.drawable.blue1);
		}else if(notified==1){
			btn_map.setBackgroundResource(R.drawable.red1);
			btn_map.startAnimation(animation);
		}
		
	}

	public void showAlertDialog(String message) {
		new AlertDialog.Builder(HomeActivity.this).setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				}).show();
	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	private boolean isMyServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if (ServiceUberGuest.class.getName().equals(
					service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
	
	public void dialogueToChanjePassword()
	{
		new AlertDialog.Builder(HomeActivity.this)
		 .setMessage("Please change your password from edit Profile")
		 .setTitle("Temporary Password")
		 .setPositiveButton("OK", new DialogInterface.OnClickListener() {
			 public void onClick(DialogInterface dialog, int which) {
				 
				 UserInfo userInfo = UserInfo.getInstance(HomeActivity.this);
				 userInfo.is_password_changed = "No";
				 userInfo.saveUserInfo(HomeActivity.this);
				 
				 Intent editProfile = new Intent(HomeActivity.this,EditProfileActivity.class);
				 startActivity(editProfile); 
			 }
		 	})
		 	.show();
		
	}
}
