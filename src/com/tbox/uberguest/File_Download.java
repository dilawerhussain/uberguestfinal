package com.tbox.uberguest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.os.AsyncTask;
import android.os.Environment;

public class File_Download extends AsyncTask<String, Void, String> {
	String filepathResult;
	String fileDestination;
	String imageURL;
	File file  = null;
	File_Download(String imageURL , String fileDestination){
		this.imageURL= imageURL;
		this.fileDestination = fileDestination;
	}
	
	@Override
	protected String doInBackground(String... arg0) {
//		file = null;
		URL url = null;
		try {
			url = new URL(imageURL);
		
		//create the new connection
			HttpURLConnection urlConnection;
			urlConnection = (HttpURLConnection) url.openConnection();
		//set up some things on the connection
		   urlConnection.setRequestMethod("GET");
		   urlConnection.setDoOutput(true); 
		    //and connect!
		   urlConnection.connect();
		   
		   
		   
		   String filepath = Environment.getExternalStorageDirectory().getPath();
           File file = new File(filepath,"uberguest");
           
           if(!file.exists()){
                   file.mkdirs();
           }
           String fileName = file.getAbsolutePath() + "/"  + fileDestination;
		   
//		   if(file.createNewFile())
//		   {
//			   file.createNewFile();
//		   }
		   
		   //this will be used to write the downloaded data into the file we created
		   FileOutputStream fileOutput = new FileOutputStream(fileName);

		   //this will be used in reading the data from the internet
		   InputStream inputStream = urlConnection.getInputStream();

		   //this is the total size of the file
		   int totalSize = urlConnection.getContentLength();
		   //variable to store total downloaded bytes
		   int downloadedSize = 0;
		   
		 //create a buffer...
		   byte[] buffer = new byte[1024];
		   int bufferLength = 0; //used to store a temporary size of the buffer

		   //now, read through the input buffer and write the contents to the file
		   while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
		    //add the data in the buffer to the file in the file output stream (the file on the sd card
		    fileOutput.write(buffer, 0, bufferLength);
		    //add up the size so we know how much is downloaded
		    downloadedSize += bufferLength;
		   
		   }
		   
		   fileOutput.close();
		   if(downloadedSize==totalSize)   
			   filepathResult=fileName;
		   
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return filepathResult;
	}

	
	

}
