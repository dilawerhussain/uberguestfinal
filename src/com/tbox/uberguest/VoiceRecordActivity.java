package com.tbox.uberguest;

import java.io.File;
import java.io.IOException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

@SuppressLint("SdCardPath")
public class VoiceRecordActivity extends Activity {
    private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
    private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
    private static final String FOLDER = "UberGuest";
    
    private MediaRecorder recorder = null;
    MediaPlayer mp = null;
    private int currentFormat = 0;
    private int output_formats[] = { MediaRecorder.OutputFormat.MPEG_4, MediaRecorder.OutputFormat.THREE_GPP };
    private String file_exts[] = { AUDIO_RECORDER_FILE_EXT_MP4, AUDIO_RECORDER_FILE_EXT_3GP }; 
    String fileName, audioPath = "";
    int value = 1;
    Context conext;
    boolean flag;
    ScreenType screenTypeObj;
    
@Override
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
	getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.activity_voice);
    
    String root = Environment.getExternalStorageDirectory().toString();
    File myDir = new File(root+"/UberGuest");
    if(!myDir.exists())
	   myDir.mkdirs();
    conext=this;
    flag = false;
    Intent intent = getIntent();
    screenTypeObj = ScreenType.detachFrom(intent);
    audioPath = Utility.getString(this, R.string.voice_defalut_path);
    setButtonHandlers();
}

    private void setButtonHandlers() {
    ((Button)findViewById(R.id.btnStart)).setOnClickListener(btnClick);
    ((Button)findViewById(R.id.btnStop)).setOnClickListener(btnClick);
    ((Button)findViewById(R.id.btnSave)).setOnClickListener(btnClick);
    ((Button)findViewById(R.id.btnPlay)).setOnClickListener(btnClick);
    
    findViewById(R.id.btnStart).setEnabled(true);
    findViewById(R.id.btnStop).setEnabled(false);
    findViewById(R.id.btnStop).setBackgroundResource(R.drawable.stop_disable);
    findViewById(R.id.btnSave).setEnabled(false);
    findViewById(R.id.btnSave).setBackgroundResource(R.drawable.save_disable);
    findViewById(R.id.btnPlay).setEnabled(false);
    findViewById(R.id.btnPlay).setBackgroundResource(R.drawable.play_disable);
    
    
    
    }
    
//    private void enableButton(int id,boolean isEnable){
//            ((Button)findViewById(id)).setEnabled(isEnable);
//    }
    
//    private void enableButtons(boolean isRecording) {
//            enableButton(R.id.btnStart,!isRecording);
//            enableButton(R.id.btnSave,!isRecording);
//            enableButton(R.id.btnStop,isRecording);
//            enableButton(R.id.btnPlay,!isRecording);
//    }
//    private void ableButtonsPlay(boolean isRecording) {
//        enableButton(R.id.btnStart,isRecording);
//        enableButton(R.id.btnSave,isRecording);
//        enableButton(R.id.btnStop,isRecording);
//        enableButton(R.id.btnPlay,isRecording);
//    }
    
//    private void enableButtonsPlay(boolean isRecording) {
//        enableButton(R.id.btnStart,!isRecording);
//        enableButton(R.id.btnSave,!isRecording);
//        enableButton(R.id.btnStop,isRecording);
//        enableButton(R.id.btnPlay,isRecording);
//    }

    
    private String getFilename(){
            String filepath = Environment.getExternalStorageDirectory().getPath();
            File file = new File(filepath,FOLDER);
            
            if(!file.exists()){
                    file.mkdirs();
            }
            fileName = "testing" + file_exts[currentFormat];
            return (file.getAbsolutePath() + "/" + fileName );
    }

    private void startRecording(){
            recorder = new MediaRecorder();
            
            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            recorder.setOutputFormat(output_formats[currentFormat]);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            recorder.setOutputFile(Utility.getString(VoiceRecordActivity.this, R.string.voice_defalut_path));
            
            
			   
            recorder.setOnErrorListener(errorListener);
            recorder.setOnInfoListener(infoListener);
            
            try {
                    recorder.prepare();
                    recorder.start();
            } catch (IllegalStateException e) {
                    e.printStackTrace();
            } catch (IOException e) {
                    e.printStackTrace();
            }
    }
    
    private void stopRecording(){
            if(null != recorder){
                    recorder.stop();
                    recorder.reset();
                    recorder.release();  
                    recorder = null;
            }
    }
    
    
    private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
            @Override
            public void onError(MediaRecorder mr, int what, int extra) {
//                    AppLog.logString("Error: " + what + ", " + extra);
            }
    };
    
    private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
            @Override
            public void onInfo(MediaRecorder mr, int what, int extra) {
//                    AppLog.logString("Warning: " + what + ", " + extra);
            }
    };

private View.OnClickListener btnClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    switch(v.getId()){
                            case R.id.btnStart:{
//                                    AppLog.logString("Start Recording");
                            		flag = true;
                            		
                            		findViewById(R.id.btnStart).setEnabled(false);
                            		findViewById(R.id.btnStart).setBackgroundResource(R.drawable.record_disable);
                            	    findViewById(R.id.btnStop).setEnabled(true);
                            	    findViewById(R.id.btnStop).setBackgroundResource(R.drawable.stop);
                            	    findViewById(R.id.btnSave).setEnabled(false);
                            	    findViewById(R.id.btnSave).setBackgroundResource(R.drawable.save_disable);
                            	    findViewById(R.id.btnPlay).setEnabled(false);
                            	    findViewById(R.id.btnPlay).setBackgroundResource(R.drawable.play_disable);
                            	    
                                    startRecording();
                                                    
                                    break;
                            }
                            case R.id.btnStop:{
//                                    AppLog.logString("Start Recording");
                            	
                            	findViewById(R.id.btnStart).setEnabled(true);
                        		findViewById(R.id.btnStart).setBackgroundResource(R.drawable.record);
                        	    findViewById(R.id.btnStop).setEnabled(false);
                        	    findViewById(R.id.btnStop).setBackgroundResource(R.drawable.stop_disable);
                        	    findViewById(R.id.btnSave).setEnabled(true);
                        	    findViewById(R.id.btnSave).setBackgroundResource(R.drawable.save);
                        	    findViewById(R.id.btnPlay).setEnabled(true);
                        	    findViewById(R.id.btnPlay).setBackgroundResource(R.drawable.play);
                        	    
                                    stopRecording();
                                    
                                    break;
                            }
                            case R.id.btnSave:{
                            	
                            	Utility.voice_path = Utility.getString(VoiceRecordActivity.this, R.string.voice_defalut_path);
                    			Utility.isVoiceRecorded=true;
                            	
                    			new AlertDialog.Builder(VoiceRecordActivity.this)
                    		    .setTitle("")
                    		    .setMessage(R.string.msg_recording_done)
                    		    .setPositiveButton(R.string.alert_ok, new DialogInterface.OnClickListener() {
                    		        public void onClick(DialogInterface dialog, int which) {
                    		        	finish();
                    		        }
                    		     })
                    		     .show();
//                        		if(screenTypeObj==ScreenType.Voice_Record_From_EditProfile){
//                        			findViewById(R.id.btnStart).setEnabled(true);
//                            	    findViewById(R.id.btnStart).setBackgroundResource(R.drawable.record);
//                            	    findViewById(R.id.btnStop).setEnabled(false);
//                            	    findViewById(R.id.btnStop).setBackgroundResource(R.drawable.stop_disable);
//                            	    findViewById(R.id.btnSave).setEnabled(true);
//                            	    findViewById(R.id.btnSave).setBackgroundResource(R.drawable.save);
//                            	    findViewById(R.id.btnPlay).setEnabled(true);
//                            	    findViewById(R.id.btnPlay).setBackgroundResource(R.drawable.play);
//                        		}else{
//	                        		SignupActivity.audioPath = getFilename();
//                        		}
                    			
                            		
                                    break;
                            }
                            case R.id.btnPlay:{
                            	mp = new MediaPlayer();
//                            		 enableButtons(true);
                            	//enableButtonsPlay(true);
                            	    findViewById(R.id.btnStart).setEnabled(true);
                            	    findViewById(R.id.btnStart).setBackgroundResource(R.drawable.record);
                            	    findViewById(R.id.btnStop).setEnabled(false);
                            	    findViewById(R.id.btnStop).setBackgroundResource(R.drawable.stop_disable);
                            	    findViewById(R.id.btnSave).setEnabled(true);
                            	    findViewById(R.id.btnSave).setBackgroundResource(R.drawable.save);
                            	    findViewById(R.id.btnPlay).setEnabled(true);
                            	    findViewById(R.id.btnPlay).setBackgroundResource(R.drawable.play); 
                            		
//                            	 MediaPlayer mp = new MediaPlayer();
                                 try {
                                mp.setDataSource(Utility.getString(VoiceRecordActivity.this, R.string.voice_defalut_path));
//                                if(screenTypeObj==ScreenType.Voice_Record_From_Signup&&flag)
//                                	mp.setDataSource(getFilename());
//                                else
//                                	mp.setDataSource(audioPath);
                                 mp.prepare();
                                 mp.start();
                                 } catch (IllegalArgumentException e) {
                                     e.printStackTrace();
                                 } catch (IllegalStateException e) {
                                     e.printStackTrace();
                                 } catch (IOException e) {
                                     e.printStackTrace();
                                 }
                                
//                                 ableButtonsPlay(true);
                                break;
                        }
                            
                    }
            }
    }; 
}
