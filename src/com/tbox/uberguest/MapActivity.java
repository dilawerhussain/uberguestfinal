package com.tbox.uberguest;

import java.util.Currency;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class MapActivity extends FragmentActivity implements LocationListener {

	private GoogleMap googleMap;
	private LocationManager locationManager;
	LatLng latLng;
	EditText txt_search;
	JSONArray properties;

	String cur_prop_name="";
	Double cur_prop_lat=0.0;
	Double cur_prop_long=0.0;
	InputMethodManager imm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_map);
		
	}
	@Override
	protected void onResume() {
		super.onResume();
		registerViews();
		attach_listeners();
		Intent i = getIntent();
		if (i.hasExtra("property_name")) {
			cur_prop_name = i.getStringExtra("property_name");
			SharedPreferences sp = getSharedPreferences("isNotified", Activity.MODE_PRIVATE);
//			cur_prop_lat = Double.parseDouble(i.getStringExtra("latitude"));
//			cur_prop_long = Double.parseDouble(i.getStringExtra("longitude"));
			cur_prop_lat = Double.parseDouble(sp.getString("cur_prop_lat", "0"));
			cur_prop_long = Double.parseDouble(sp.getString("cur_prop_long", "0"));
			
			Log.e("MapBefore",""+ sp.getInt("notified", 0));
			SharedPreferences.Editor editor = sp.edit();
	        editor.putInt("notified", 0);
	        editor.commit();
	        Log.e("MapAfter",""+ sp.getInt("notified", 0));
		}
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 100, 100, this);
		try {
			// Loading map
			initilizeMap();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void attach_listeners() {
		// TODO Auto-generated method stub
		
		txt_search
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_SEARCH) {
							// txt_search.setCursorVisible(true);
							searchName(txt_search.getText().toString());
							return true;
						}
						return false;
					}

				});
		txt_search.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				txt_search.setCursorVisible(true);
			}
		});

	}

	protected void searchName(String search) {
		// TODO Auto-generated method stub

		RequestParams params = new RequestParams();
		params.put(Constants.kapi_key,
				UserInfo.getInstance(MapActivity.this).api_key);
		params.put("name", search);

		// send post request for user login
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(Constants.api_base_url + Constants.guest_search_property,
				params, new JsonHttpResponseHandler() {
					@SuppressLint("InlinedApi")
					@Override
					public void onFailure(Throwable throwable,
							JSONObject jsonObject) {
						int status = 0;

						try {
							status = jsonObject.getInt("status");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Log.e("Status", "" + status);

						super.onFailure(throwable, jsonObject);
					}

					@Override
					public void onSuccess(final JSONObject object) {
						int status = 0;
						try {
							status = object.getInt("status");
							if (status == 1) {
								properties = object.getJSONArray("response");
								if (properties.length() > 0) {
									googleMap.clear();
									MarkerOptions marker = new MarkerOptions();
									for (int i = 0; i < properties.length(); i++) {
										marker = new MarkerOptions()
												.position(
														new LatLng(
																Double.parseDouble(properties
																		.getJSONObject(
																				i)
																		.getString(
																				"latitude")),
																Double.parseDouble(properties
																		.getJSONObject(
																				i)
																		.getString(
																				"longitude"))))
												.title(properties
														.getJSONObject(i)
														.getString(
																"property_name"));
										marker.icon(BitmapDescriptorFactory
												.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
										googleMap.addMarker(marker);
									}
									
									CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(marker.getPosition());
									CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);
									googleMap.moveCamera(cameraUpdate);
								    googleMap.animateCamera(zoom);
									//imm.hideSoftInputFromWindow(txt_search.getWindowToken(), 0);
								}
							}
							Log.e("Status", "" + status);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
		InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(txt_search.getWindowToken(), 0);
	}

	private void registerViews() {
		// TODO Auto-generated method stub
		txt_search = (EditText) findViewById(R.id.txt_search);
		InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
	}

	private void initilizeMap() {

		if (googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
			if (!cur_prop_name.isEmpty()) 
			{
				MarkerOptions marker1 = new MarkerOptions().position(new LatLng(cur_prop_lat, cur_prop_long)).title(cur_prop_name);
				marker1.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
				googleMap.addMarker(marker1);
				
			}
			MarkerOptions marker1 = new MarkerOptions().position(new LatLng(cur_prop_lat, cur_prop_long)).title(cur_prop_name);
			marker1.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
			googleMap.addMarker(marker1);
			googleMap.setMyLocationEnabled(true);
			googleMap.getUiSettings().setZoomControlsEnabled(true); // true to
			LatLng coordinate = new LatLng(cur_prop_lat, cur_prop_long);
			CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 10);
			googleMap.animateCamera(yourLocation);										// enable
			googleMap.getMyLocation();
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

			// adding marker
			
			if (googleMap == null) {
				Toast.makeText(getApplicationContext(),
						"Sorry! unable to create maps", Toast.LENGTH_SHORT)
						.show();
			}
			googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
				
				@Override
				public void onInfoWindowClick(Marker marker) {
					// TODO Auto-generated method stub
					String uri = String.format(Locale.ENGLISH, "geo:%f,%f", marker.getPosition().latitude, marker.getPosition().longitude);
					Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
					startActivity(intent);
					
				}
			});
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}

	@Override
	public void onLocationChanged(Location loc) {
		// TODO Auto-generated method stub
		latLng = new LatLng(loc.getLatitude(), loc.getLongitude());
		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng,12);
		googleMap.animateCamera(cameraUpdate);
//		Toast.makeText(getApplicationContext(), "Lat Long Set",
//				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
//		Intent intent=new Intent(getApplicationContext(), HomeActivity.class);
//		intent.setFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
//		startActivity(intent);
		Intent intent_home = new Intent(MapActivity.this,HomeActivity.class);
		 ScreenType.Home_From_Login.attachTo(intent_home);
		 intent_home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		 startActivity(intent_home);
		 finish();
		this.finish();
        //imm.hideSoftInputFromWindow(txt_search.getWindowToken(), 0);
//		super.onBackPressed();
	}

}
