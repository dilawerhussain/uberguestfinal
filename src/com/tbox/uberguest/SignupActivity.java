package com.tbox.uberguest;


import android.telephony.TelephonyManager;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.makeramen.RoundedImageView;
import com.tbox.backend.FileUploader;
import com.tbox.backend.FileUploaderInterface;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Scroller;

public class SignupActivity extends Activity implements FileUploaderInterface,LocationListener{
	EditText 		et_first_name , et_last_name , et_country , et_city , et_email , et_password,et_title,et_reason,et_state,
		et_like,et_dislike,et_spouse,et_birthday,et_company,et_cell_number,et_special_instruction,et_confirm_password,et_answer;
	
	private LocationListener locationListener = null;
	Button				btn_gallery, btn_login, btn_camera,btn_record;
	Button        		btn_register;
	ProgressDialog 		progressDialog;
	Bitmap 				bitmap;
	public static LocationManager locationMangaer = null;
	static String 		audioPath = "";
	static String 		picturePath="";
	RoundedImageView    img_profile;
    String 				ime;
    double 				longitude,latitude;
    final Context 		context = this;


    private void registerAllElements() {
    	
		// TODO Auto-generated method stub
//		btn_gallery 	= (Button) findViewById(R.id.imageUpLoad);
        btn_register 	= (Button) findViewById(R.id.register);
        btn_record 		= (Button) findViewById(R.id.btnVoiceRecord);
//        btn_camera 		= (Button)findViewById(R.id.imageCamera);
        
        et_first_name 	= (EditText)findViewById(R.id.userFirstName);
        et_last_name 	= (EditText)findViewById(R.id.userLastName);
        et_email 		= (EditText)findViewById(R.id.userEmail);
        et_password 	= (EditText)findViewById(R.id.userPassword);
        et_confirm_password = (EditText)findViewById(R.id.confirm_userPassword);
        
        et_city 		= (EditText)findViewById(R.id.user_city_sign_up);
        et_state 		= (EditText)findViewById(R.id.user_state_sign_up);
        et_country 		= (EditText)findViewById(R.id.user_country_sign_up);
        et_like 		= (EditText)findViewById(R.id.user_like);
        et_dislike		= (EditText)findViewById(R.id.user_dislike);
//        et_spouse		= (EditText)findViewById(R.id.user_spouse);
        et_birthday		= (EditText)findViewById(R.id.user_birthday);
        et_cell_number	= (EditText)findViewById(R.id.user_cell_number);
        et_special_instruction	= (EditText)findViewById(R.id.user_special_instruction);
        
        et_birthday.setMaxLines(3);
//        et_spouse.setMaxLines(3);
        et_special_instruction.setMaxLines(3);
        et_special_instruction.setScroller(new Scroller(this)); 
        et_special_instruction.setVerticalScrollBarEnabled(true); 
        et_special_instruction.setMovementMethod(new ScrollingMovementMethod()); 
        
        et_dislike.setMaxLines(3);
        et_like .setScroller(new Scroller(this)); 
        et_like .setMaxLines(3); 
        et_like .setVerticalScrollBarEnabled(true); 
        et_like .setMovementMethod(new ScrollingMovementMethod()); 
        et_answer 		= (EditText)findViewById(R.id.user_security_question);
        et_title 		= (EditText)findViewById(R.id.user_tile);
        et_company      = (EditText)findViewById(R.id.user_company);
        et_company.setMovementMethod(new ScrollingMovementMethod());
        img_profile 		= (RoundedImageView) findViewById(R.id.imageView);
      
	}
	
	private void attachListeners(){

       
		img_profile.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	
            	
            	Intent intent = new Intent(SignupActivity.this,CustomCameraActivity.class);
            	startActivity(intent);
            	
             }
        });
           
        btn_record.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	Intent intent_sign=new Intent(SignupActivity.this,VoiceRecordActivity.class);
				ScreenType.Voice_Record_From_Signup.attachTo(intent_sign);
				startActivity(intent_sign);
            }
        });
        
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
            	locationMangaer.removeUpdates(locationListener);
            	if(Utility.isImageCaptured==false){
            		Utility.showAlertDialog(Utility.getString(SignupActivity.this, R.string.image_mandatory_message),"Image is required", SignupActivity.this);
            		return;
            	}else if (Utility.isVoiceRecorded == false){
            		Utility.showAlertDialog(Utility.getString(SignupActivity.this, R.string.voice_mandatory_message),"Voice is required", SignupActivity.this);
            		return;
            	}     	
            	if(Utility.isDataValid(et_email.getText().toString(),et_password.getText().toString(),et_confirm_password.getText().toString(),et_first_name.getText().toString(),et_last_name.getText().toString()
            			,et_answer.getText().toString(),SignupActivity.this))
            	{
            				// add user data as params
            				JSONObject json = new JSONObject();
        			try {
        				json.put(Constants.kfirst_name,et_first_name.getText().toString());
        				json.put(Constants.klast_name,et_last_name.getText().toString());
        				json.put(Constants.kemail_id,et_email.getText().toString());
        				json.put("password",et_password.getText().toString());
        				json.put(Constants.klatitude,Double.toString(latitude));
        				json.put(Constants.klongitude,Double.toString(longitude));
        				json.put(Constants.kcity,et_city.getText().toString());
        				json.put(Constants.kstate,et_state.getText().toString());
        				json.put(Constants.kcountry,et_country.getText().toString());
        				json.put(Constants.ktitle,et_title.getText().toString());
        				json.put(Constants.klike,et_like.getText().toString());
        				json.put(Constants.kcompany,et_company.getText().toString());
        				json.put(Constants.k_answer,et_answer.getText().toString());
        				json.put(Constants.kdislike,et_dislike.getText().toString());
//        				json.put(Constants.kspouse,et_spouse.getText().toString());
        				json.put(Constants.kbirthday,et_birthday.getText().toString());
        				json.put(Constants.kcell_number,et_cell_number.getText().toString());
        				json.put(Constants.kspecial_instruction,et_special_instruction.getText().toString());
                        json.put("device_id",ime);

                    } catch (JSONException e) {
        				// TODO Auto-generated catch block
        				e.printStackTrace();
        			}
        			/////put all data in a parameter 
        			RequestParams params = new RequestParams();
        	        params.put("data", json.toString());

        	        progressDialog = ProgressDialog.show(SignupActivity.this, "", Utility.getString(SignupActivity.this, R.string.signup_waiting));
        	        progressDialog.setCancelable(false);
        	        
        	        //send post request for user signup
        	    	AsyncHttpClient client = new AsyncHttpClient();
        	    	client.post(Constants.api_base_url+Constants.guest_register, params , new JsonHttpResponseHandler(){
        	    		@Override
                        public void onSuccess(final JSONObject object){
        	    			 int status = 0;
        						try {
        							status = object.getInt("status");
        						} catch (JSONException e) {
        							// TODO Auto-generated catch block
        							e.printStackTrace();
        						}
        		            	 if(status ==1){
        							try {
        								
        								 UserInfo userInfo = UserInfo.getInstance(SignupActivity.this);
        			            		 userInfo.api_key = object.getString("api_key");
        			            		 
        			            		 userInfo.first_name = et_first_name.getText().toString();
        			            		 userInfo.last_name = et_last_name.getText().toString();
        			            		 userInfo.email_id = et_email.getText().toString();
        			            		 userInfo.city = et_city.getText().toString();
        			            		 userInfo.state = et_state.getText().toString();
        			            		 userInfo.security_answer = et_answer.getText().toString();
        			            		 userInfo.country = et_country.getText().toString();
        			            		 userInfo.password = et_password.getText().toString();
        			            		 userInfo.like = et_like.getText().toString();
        			            		 userInfo.title = et_title.getText().toString();
        			            		 userInfo.company = et_company.getText().toString();
        			            		 userInfo.dislike = et_dislike.getText().toString();
        			            		 userInfo.latitude = Double.toString(latitude);
        			            		 userInfo.longitude = Double.toString(longitude);
        			            		 userInfo.is_password_changed = "No";
//        			            		 userInfo.spouse = et_spouse.getText().toString();
        			            		 userInfo.birthday = et_birthday.getText().toString();
        			            		 userInfo.cell_number = et_cell_number.getText().toString();
        			            		 userInfo.Special_Instruction = et_special_instruction.getText().toString();
        			            		 userInfo.profile_image_path = picturePath;
        			            		 userInfo.name_recording_path = audioPath;
        			            		 
        			            		 //save user data to share preferences
        			            		 userInfo.saveUserInfo(SignupActivity.this);
        			            		 if(Utility.isImageCaptured == true){
        				            			new FileUploader(SignupActivity.this, Utility.profile_image_path,Constants.api_base_url+Constants.guest_image_upload +userInfo.api_key,"image","image/jpeg",0).execute();
        				            		}
        				            		if(Utility.isVoiceRecorded == true){
        				            			new FileUploader(SignupActivity.this, Utility.voice_path,Constants.api_base_url+"upload_guest_voice/"+userInfo.api_key,"voice","audio/mp4",1).execute();
        				            		}
        			            	} catch (JSONException e) {
        								// TODO Auto-generated catch block
        								e.printStackTrace();
        							}
        						}
        	    		}
       	             @Override
    	             protected void handleFailureMessage(Throwable e, String responseBody) {
    	 				try{
    						JSONObject jobject =  new JSONObject(responseBody);
    						int status = jobject.getInt("status");
    						if(status ==0){
//    							UserInfo userInfo= UserInfo.getInstance(context);	
    							Utility.showAlertDialog(jobject.getString("error_message"),"Sorry", SignupActivity.this);
//    							userInfo.saveUserInfo(context);
    						}
    					} catch (JSONException ex) {
    						// TODO Auto-generated catch block
    						ex.printStackTrace();
    					}
    	 				progressDialog.dismiss();
       	             }

        	    	});
            }
            }
        });
	}
	@Override
	public void onPause()
	{
		super.onPause();
		Constants.temp_image_gallery = 0;
		locationMangaer.removeUpdates(locationListener);
	}
	@Override
	public void onResume()
	{
		super.onResume();
		if(Constants.temp_image_gallery == 1)
		{
			try{
				
				Bitmap myBitmap = BitmapFactory.decodeFile(Utility.profile_image_path);
				img_profile.setImageBitmap(myBitmap);
				
				}catch(Exception E)
				{}
		}
		else if(Constants.temp_image_gallery == 2)
		{
			Bitmap myBitmap = BitmapFactory.decodeFile(Utility.profile_image_path);
			CustomCameraActivity.is_back_camera = true;
			img_profile.setImageBitmap(myBitmap);
		}

	}
	
	@SuppressWarnings("static-access")
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.signup_activity);
        locationMangaer = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = new SignupActivity();
        locationMangaer.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 0, locationListener);
        if (!displayGpsStatus(SignupActivity.this))
		{
        	displayPromptForEnablingGPS(SignupActivity.this);
		}
		
		
        registerAllElements();
        attachListeners();
        Bitmap bitmap= BitmapFactory.decodeResource(context.getResources(), R.drawable.camera);
        img_profile.setImageBitmap(bitmap);
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(this.TELEPHONY_SERVICE);
        ime=telephonyManager.getDeviceId();
        Log.e("Unique ID", "IME" + telephonyManager.getDeviceId());
	}
	
	@Override
	public void fileUploaded(FileUploader fileUploader) {
		// TODO Auto-generated method stub
		if(Utility.isImageCaptured == true){
			Utility.isImageCaptured=false;
			if(Utility.isVoiceRecorded !=true){
				progressDialog.dismiss();
				Intent intent_home = new Intent(SignupActivity.this,HomeActivity.class);
       		 	ScreenType.Home_Default.attachTo(intent_home);
       		 	intent_home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
       		 	startActivity(intent_home);
       		 	finish();
			}
		}
		if(Utility.isVoiceRecorded == true){
			Utility.isVoiceRecorded=false;
			if(Utility.isImageCaptured !=true){
				progressDialog.dismiss();
				Intent intent_home = new Intent(SignupActivity.this,HomeActivity.class);
       		 	ScreenType.Home_Default.attachTo(intent_home);
       		 	intent_home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
       		 	startActivity(intent_home);
       		 	finish();
			}
		}
		
	}

	@Override
	public float uploadProgress(FileUploader fileUploader,
			int downloadProgress) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		longitude = location.getLongitude();
		latitude = location.getLatitude();
		Log.e("lat long", longitude+"--");
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
	public static void displayPromptForEnablingGPS(final Context ctx) {

		final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
		final String message = "Do you want open GPS setting?";

		builder.setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface d, int id) {
						
						if (locationMangaer.getAllProviders().contains("gps")) 
						{
							Log.e("GPS", "Device GPS Enabled "
									+ locationMangaer.getAllProviders().contains("gps"));
							ctx.startActivity(new Intent(action));
							d.dismiss();
						}
						else 
						{
							Log.e("GPS", "Device GPS Enabled "
									+ locationMangaer.getAllProviders().contains("gps"));
							Utility.showAlertDialog("Sorry your device has no gps ..","Sorry", ctx);
						}
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface d, int id) {
								d.cancel();
							}
						});
		builder.create().show();
	}
	public Boolean displayGpsStatus(Context context) {
		ContentResolver contentResolver = SignupActivity.this
				.getContentResolver();
		boolean gpsStatus = Settings.Secure.isLocationProviderEnabled(
				contentResolver, LocationManager.GPS_PROVIDER);
		if (gpsStatus) {
			return true;

		} else {
			return false;
		}
	}
}
