package com.tbox.uberguest;


import android.telephony.TelephonyManager;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;


public class LoginActivity extends Activity implements OnClickListener{
	EditText 	et_email,et_password,txt_username_for_forget_password;
	Button 		btn_login, btn_signup,btn_reset_forget_password,invalid_email_button;
	int login_attempt = 0;
	ProgressBar spinner;
    String android_id,is_password_changed;
    String ime;

	private void registerAllElements(){
		et_email 	= (EditText) findViewById(R.id.user_id_text);
	    et_password = (EditText) findViewById(R.id.user_password_text);
	    btn_login 	= (Button)findViewById(R.id.btnRegister);
	    btn_signup 	= (Button)findViewById(R.id.sigUpLogin);
	    spinner 	= (ProgressBar)findViewById(R.id.progressBar1);
	}
	
	private void attachListeners(){
		btn_login.setOnClickListener(this);
	    btn_signup.setOnClickListener(this);
	}
	private void openHomeActivityIfLogin(){
		UserInfo userInfo = UserInfo.getInstance(LoginActivity.this);
		//check if login
		if(userInfo.is_login==true){
			Intent home_intent=new Intent(LoginActivity.this,HomeActivity.class);
			ScreenType.Home_Default.attachTo(home_intent);
			startActivity(home_intent);
			finish();

		}
	}
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    	getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.login_activity);
       
        
        registerAllElements();
        attachListeners();
	    openHomeActivityIfLogin();
//        android_id = Secure.getString(this.getContentResolver(),
//                Secure.ANDROID_ID);
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(this.TELEPHONY_SERVICE);
        ime=telephonyManager.getDeviceId();
        Log.e("Unique ID", "IME"+telephonyManager.getDeviceId());
    }
    @Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		btn_login.setEnabled(true);
		UserInfo userInfo = UserInfo.getInstance(LoginActivity.this);
		try{
			if(userInfo.email_id.length()>0){
				et_email.setText(userInfo.email_id);
				et_password.setText(userInfo.password);
			}
		}catch (NullPointerException e) {
			// TODO: handle exception
		}
		
		
	}
    

	@SuppressLint("ShowToast")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()){
		case R.id.btnRegister:
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	        imm.hideSoftInputFromWindow(et_password.getWindowToken(), 0);
		btn_login.setEnabled(false);
		if(Utility.isLoginValid(et_email.getText().toString(), et_password.getText().toString(), this))
		{
			spinner.setVisibility(View.VISIBLE);
			// add user data as params
			JSONObject json = new JSONObject();
			try {
				json.put("email_id",et_email.getText().toString());
				json.put("password",et_password.getText().toString());
                json.put("device_id",ime);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

			RequestParams params = new RequestParams();
	        params.put("data", json.toString());

//	        Utility.showAlertDialog("data to send:"+json.toString(), LoginActivity.this);
	        //send post request for user login
	    	AsyncHttpClient client = new AsyncHttpClient();
	    	client.post(Constants.api_base_url+Constants.guest_login, params , new JsonHttpResponseHandler(){  

	             @Override
	             public void onSuccess(final JSONObject object){
	            	 int status = 0;
					try {
						status = object.getInt("status");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
//					Utility.showAlertDialog("data to send:"+object.toString(), LoginActivity.this);
	            	 if(status ==1){
	            		 JSONObject userData;
	            		 try {
	            			 Log.e("response string", object.toString());
							userData = object.getJSONObject("user");
							UserInfo userInfo = UserInfo.getInstance(LoginActivity.this);
		            		userInfo.api_key = userData.getString(Constants.kapi_key);
		            		userInfo.first_name = userData.getString(Constants.kfirst_name);
		            		userInfo.last_name = userData.getString(Constants.klast_name);
		            		userInfo.email_id = userData.getString(Constants.kemail_id);
		            		userInfo.city = userData.getString(Constants.kcity);
		            		userInfo.state = userData.getString(Constants.kstate);
		            		userInfo.security_answer = userData.getString("s_answer");
		            		userInfo.country = userData.getString(Constants.kcountry);
		            		userInfo.title = userData.getString(Constants.ktitle);
		            		
		            		//userInfo.reasson = userData.getString(Constants.kreason);
		            		userInfo.like = userData.getString(Constants.klike);
		            		userInfo.company = userData.getString(Constants.kcompany);
		            		userInfo.dislike = userData.getString(Constants.kdislike);
		            		userInfo.spouse = userData.getString(Constants.kspouse);
		            		userInfo.birthday = userData.getString(Constants.kbirthday);
		            		userInfo.Special_Instruction = userData.getString(Constants.kspecial_instruction);
		            		userInfo.cell_number = userData.getString(Constants.kcell_number);
		            		userInfo.profile_image_path = userData.getString(Constants.kprofile_image_path);
		            		userInfo.name_recording_path = userData.getString(Constants.kvoice);
		            		userInfo.password = et_password.getText().toString();
		            		userInfo.is_password_changed = userData.getString(Constants.kis_passwordchanged);
		            		Log.e("is password changed", userData.getString(Constants.kis_passwordchanged)+" ");
		            		//save user data to share preferences
		            		userInfo.saveUserInfo(LoginActivity.this);
		            		btn_login.setEnabled(true);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	            		 
	            		 Intent intent_home = new Intent(LoginActivity.this,HomeActivity.class);
	            		 ScreenType.Home_From_Map.attachTo(intent_home);
	            		 startActivity(intent_home);
	            		 finish();
					}else{
						spinner.setVisibility(View.INVISIBLE);
						
					}
	             }
	            
	             @Override
	             protected void handleFailureMessage(Throwable e, String responseBody) {
	            	 spinner.setVisibility(View.INVISIBLE);
	 				try{
						JSONObject jobject =  new JSONObject(responseBody);
						int status = jobject.getInt("status");
						if(status ==0){
                            Log.e("error response",jobject.getString("error_message").toString());
                            if(jobject.getString("error_message").toString().equals("Please re-enter your password and try again"))
                            		{
                            			if(login_attempt == 0)
                            			{
                            				login_attempt = 1;
                            				btn_login.setEnabled(true);
//                            				Utility.showAlertDialog("Please re-enter your password and try again","Sorry, That Didn't Work", LoginActivity.this);
                            				final AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                            				builder.setMessage("Please re-enter your password and try again")
                            				.setTitle("Sorry, That Didn't Work")
                            						.setPositiveButton("Try Again",
                            								new DialogInterface.OnClickListener() {
                            									public void onClick(DialogInterface d, int id) {
                            									
                            									}
                            								});
                            				builder.create().show();
                            			}
                            			else
                            			{
                            				btn_login.setEnabled(true);
                            				final AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                            				builder.setMessage("I Forgot My Password")
                            				.setTitle("Login Help")
                            						.setPositiveButton("Password",
                            								new DialogInterface.OnClickListener() {
                            									public void onClick(DialogInterface d, int id) {
                            									Intent intent = new Intent(LoginActivity.this, RetreivePasswordActivity.class);
                            									startActivity(intent);
                            									}
                            								});
                            				builder.create().show();
                            			}
                            		}

                        }
					} catch (JSONException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}
	             }
	    	});
			}
		btn_login.setEnabled(true);
	    	break;
		case R.id.sigUpLogin:
			Intent intent_signup = new Intent(LoginActivity.this , SignupActivity.class);
			startActivity(intent_signup);
			finish();
			break;
			
		
		}
		
	}
	
  }