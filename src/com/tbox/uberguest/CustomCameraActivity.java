package com.tbox.uberguest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;

@SuppressLint("InflateParams")
public class CustomCameraActivity extends Activity  {

	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	public static boolean is_back_camera = true;
	public static boolean snap_orientation_portrait = true;
	static int ios = 1;
	private Camera.Size mPreviewSize;
	String 			picturePath="";
	Bitmap 			bitmap;
	Camera camera;
	SurfaceView surfaceView;
	SurfaceHolder surfaceHolder;
	boolean previewing = false;
	private Camera mCamera;
    private CameraPreview mPreview;
    LayoutInflater controlInflater = null;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.custom_camera_activity);
    
        openCamerPreview();
     }
    
   private void openCamerPreview()
   {
	   final Button captureButton = (Button) findViewById(R.id.button_capture);
       final Button gallery_button = (Button) findViewById(R.id.button_gallery);
       Button reverse_button = (Button) findViewById(R.id.camera_reverse);
       Button cancel_button = (Button) findViewById(R.id.button_cancel);
       
    // Create an instance of Camera
       mCamera = getCameraInstance();
       
       captureButton.setOnClickListener(
           new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   // get an image from the camera
            	   
            	   Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
            	   int or =display.getOrientation();
            	   if(Configuration.ORIENTATION_PORTRAIT ==or){
            		   CustomCameraActivity.snap_orientation_portrait = true;
            	   }else if (Configuration.ORIENTATION_LANDSCAPE==or){
            		   CustomCameraActivity.snap_orientation_portrait = false;
            	   }
            	   
            	   captureButton.setEnabled(false);
                   mCamera.takePicture(shutterCallback , null, mPicture);
               }
           }
       );
       
       reverse_button.setOnClickListener(
               new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                      CameraInfo info = new CameraInfo();
             	      Camera.getCameraInfo(0, info);
             	      if (info.facing == CameraInfo.CAMERA_FACING_FRONT) 
             	      {

             	    	is_back_camera = true;
             	    	Constants.temp_image_gallery = 2;
             	      }
             	      else
             	      {
             	    	setContentView(R.layout.custom_camera_activity);
                   	attachListeners();
                   	mCamera.release();        // release the camera for other applications
                    mCamera = null;
                   	mCamera = getFrontCameraInstance();
                   	mCamera.setDisplayOrientation(90);
                   	// Create our Preview view and set it as the content of our activity.
                   	mPreview = new CameraPreview(CustomCameraActivity.this, mCamera);
       	        	FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
           	        preview.addView(mPreview);
           	        Constants.temp_image_gallery = 2;
           	        is_back_camera = false;
             	      }
                   }
               }
           );
       
       gallery_button.setOnClickListener(
           new View.OnClickListener() {
               @Override
               public void onClick(View v) {
               	Intent pickPhoto = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					startActivityForResult(pickPhoto , 1);
               }
           }
       );
       cancel_button.setOnClickListener(
               new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                   	mCamera.release();
                   	finish();
                   }
               }
           );
       
       mCamera.setDisplayOrientation(90);
       //Create our Preview view and set it as the content of our activity.
       mPreview = new CameraPreview(this, mCamera);
       FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
       preview.addView(mPreview);
   }
   @Override 
   public void onResume()
    {
	   super.onResume();
    }
    public void attachListeners()
    {
    	final Button captureButton = (Button) findViewById(R.id.button_capture);
        final Button gallery_button = (Button) findViewById(R.id.button_gallery);
        Button reverse_button = (Button) findViewById(R.id.camera_reverse);
        Button cancel_button = (Button) findViewById(R.id.button_cancel);
        
        captureButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get an image from the camera
                    	captureButton.setEnabled(false);
                        mCamera.takePicture(shutterCallback , null, mPicture);
                    }
                }
            );
            
            reverse_button.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        	CameraInfo info = new CameraInfo();
                    	    Camera.getCameraInfo(ios, info);
                        	 if (info.facing == CameraInfo.CAMERA_FACING_FRONT)
                     	      {
                     	    	Log.e("Hello", "Backcamera");
                     	    	setContentView(R.layout.custom_camera_activity);
                             	attachListeners();
                             	mCamera.release();        // release the camera for other applications
                                 mCamera = null;
                             	mCamera = getBackCameraInstance();
                             	mCamera.setDisplayOrientation(90);
                             	// Create our Preview view and set it as the content of our activity.
                             	mPreview = new CameraPreview(CustomCameraActivity.this, mCamera);
                 	        	FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
                     	        preview.addView(mPreview);
                     	        Constants.temp_image_gallery = 2;
                     	        is_back_camera = true;
                     	        ios = 0;
                     	      }
                        	 else if (info.facing == CameraInfo.CAMERA_FACING_BACK)
                        	 {
                        		 Log.e("Hello", "Frontcamera");
                      	    	setContentView(R.layout.custom_camera_activity);
                              	attachListeners();
                              	mCamera.release();        // release the camera for other applications
                                  mCamera = null;
                              	mCamera = getFrontCameraInstance();
                              	mCamera.setDisplayOrientation(90);
                              	// Create our Preview view and set it as the content of our activity.
                              	mPreview = new CameraPreview(CustomCameraActivity.this, mCamera);
                  	        	FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
                      	        preview.addView(mPreview);
                      	        Constants.temp_image_gallery = 2;
                      	        is_back_camera = false;
                      	        ios = 1;
                        	 }
                        }
                    }
                );
            
          gallery_button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    	Intent pickPhoto = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    					startActivityForResult(pickPhoto , 1);
                    }
                }
            );
            cancel_button.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        	mCamera.release();
                        	finish();
                        }
                    }
                );
        
    }
    
    private final ShutterCallback shutterCallback = new ShutterCallback() {
        public void onShutter() {
            AudioManager mgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            mgr.playSoundEffect(AudioManager.FLAG_PLAY_SOUND);
        }
    };
    PictureCallback mPicture = new PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            if (pictureFile == null)
            {
//                Log.d(TAG, "Error creating media file, check storage permissions: " +
//                    e.getMessage());
            	
            	return;
                
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                //Log.d(TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
               // Log.d(TAG, "Error accessing file: " + e.getMessage());
            }
            finally{
            	mCamera.release();
            	Intent intent = new Intent(CustomCameraActivity.this, CropActivity.class);
                startActivity(intent);
            	finish();
            }
        }
    };

    /** Create a File for saving an image or video */
    @SuppressLint("SdCardPath")
	private static File getOutputMediaFile(int type){
    	
    	

//		try {
//			if(f.exists())
//				f.delete();
//			
//			f.createNewFile();
//			FileOutputStream fo = new FileOutputStream(f);
//			fo.write(bytes.toByteArray());
//			fo.close();
//			CustomCameraActivity.is_back_camera = true;
//			Utility.profile_image_path = f.getAbsolutePath().toString();
//			Utility.isImageCaptured=true;
//		} catch (Exception E) {
//		}
    	
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

//        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
//                  Environment.DIRECTORY_PICTURES), "MyCameraApp");
//        // This location works best if you want the created images to be shared
//        // between applications and persist after your app has been uninstalled.
//
//        // Create the storage directory if it does not exist
//        if (! mediaStorageDir.exists()){
//            if (! mediaStorageDir.mkdirs()){
//                Log.d("MyCameraApp", "failed to create directory");
//                return null;
//            }
//        }
//
//        // Create a media file name
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//        File mediaFile;
    	File f = null;
        if (type == MEDIA_TYPE_IMAGE)
        {
        	String filePath = "/UberGuest/temp_profile_image.jpg";
            f= new File(Environment.getExternalStorageDirectory().getPath(), filePath);
            if(f.exists())
				f.delete();
            Constants.image_name = f;
            Constants.temp_image_gallery = 2;
        }
        else {
            return null;
        }
        
        return f;
    }
    
    /** Check if this device has a camera */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }
    
    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
        	c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }
    
    public static Camera getFrontCameraInstance(){
        Camera c = null;
        try {
        
        	int cameraId = -1;
    	    // Search for the front facing camera
    	    int numberOfCameras = Camera.getNumberOfCameras();
    	    for (int i = 0; i < numberOfCameras; i++)
    	    {
    	      CameraInfo info = new CameraInfo();
    	      Camera.getCameraInfo(i, info);
    	      if (info.facing == CameraInfo.CAMERA_FACING_FRONT) 
    	      {
    	        //Log.d(DEBUG_TAG, "Camera found");
    	        cameraId = i;
    	        break;
    	      }
    	    }
        	is_back_camera = false;
            c = Camera.open(cameraId); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }
    
    public static Camera getBackCameraInstance(){
        Camera c = null;
        try {
        
        	int cameraId = -1;
    	    // Search for the front facing camera
    	    int numberOfCameras = Camera.getNumberOfCameras();
    	    for (int i = 0; i < numberOfCameras; i++)
    	    {
    	      CameraInfo info = new CameraInfo();
    	      Camera.getCameraInfo(i, info);
    	      if (info.facing == CameraInfo.CAMERA_FACING_BACK) 
    	      {
    	        //Log.d(DEBUG_TAG, "Camera found");
    	        cameraId = i;
    	        break;
    	      }
    	    }
        	is_back_camera = true;
            c = Camera.open(cameraId); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }
    
    /** A basic Camera preview class */
    public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
        private SurfaceHolder mHolder;
        private Camera mCamera;

        public CameraPreview(Context context, Camera camera) {
            super(context);
            mCamera = camera;

            // Install a SurfaceHolder.Callback so we get notified when the
            // underlying surface is created and destroyed.
            mHolder = getHolder();
            mHolder.addCallback(this);
            // deprecated setting, but required on Android versions prior to 3.0
            mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

        public void surfaceCreated(SurfaceHolder holder) {
            // The Surface has been created, now tell the camera where to draw the preview.
            try {

                
                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
            } catch (IOException e) {
                Log.e("error", "Error setting camera preview: " + e.getMessage());
            }
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            // empty. Take care of releasing the Camera preview in your activity.
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
            // If your preview can change or rotate, take care of those events here.
            // Make sure to stop the preview before resizing or reformatting it.

            if (mHolder.getSurface() == null){
              // preview surface does not exist
              return;
            }

            // stop preview before making changes
            try {
                mCamera.stopPreview();
            } catch (Exception e){
              // ignore: tried to stop a non-existent preview
            }

            // set preview size and make any resize, rotate or
            // reformatting changes here

            // start preview with new settings
            try {
                
                mCamera.setPreviewDisplay(mHolder);
                mCamera.startPreview();

            } catch (Exception e){
                Log.e("error starting camera", "Error starting camera preview: " + e.getMessage());
            }
        }
        
       
        
        
        public class CameraActivity extends Activity {
            private Camera mCamera;
            private SurfaceView mPreview;
            private MediaRecorder mMediaRecorder;


            @Override
            protected void onPause() {
                super.onPause();
                releaseMediaRecorder();       // if you are using MediaRecorder, release it first
                releaseCamera();              // release the camera immediately on pause event
            }

            private void releaseMediaRecorder(){
                if (mMediaRecorder != null) {
                    mMediaRecorder.reset();   // clear recorder configuration
                    mMediaRecorder.release(); // release the recorder object
                    mMediaRecorder = null;
                    mCamera.lock();           // lock camera for later use
                }
            }

            private void releaseCamera(){
                if (mCamera != null){
                    mCamera.release();        // release the camera for other applications
                    mCamera = null;
                }
            }
        }
    }
  @Override
  public void onBackPressed()
  {
	 mCamera.release();
	  finish();
  }
  
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);
      switch(requestCode){

	case 1:
		if(requestCode == 1 && resultCode == RESULT_OK){  
			
			Uri selectedImage = data.getData();
	          HomeActivity.selectedImage = selectedImage;
	          String[] filePathColumn = { MediaStore.Images.Media.DATA };
	          Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
	          cursor.moveToFirst();
	          int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	          picturePath = cursor.getString(columnIndex);
	          
	          Constants.picturePath = picturePath;
	          
	          Log.e("pic path", picturePath);
	          cursor.close();
	          Constants.temp_image_gallery = 1;
              mCamera.release();
            
              Constants.image_name = new File(picturePath);
              
            Intent intent = new Intent(CustomCameraActivity.this, CropActivity.class);
            startActivity(intent);
            
            
            finish();
		 
		}

      break;
      }
	}
}
