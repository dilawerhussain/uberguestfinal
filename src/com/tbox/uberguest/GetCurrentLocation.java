package com.tbox.uberguest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public final class GetCurrentLocation  {
	
	public LocationManager locationMangaer=null;
	private LocationListener locationListener=null;	
	String longitude="",latitude="";
	boolean status = false;
	private Button btnGetLocation = null;
	private EditText editLocation = null;	
	private ProgressBar pb =null;
	
	private static final String TAG = "Debug";
	private Boolean flag = false;

//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.location_activity);
//		
//		
//		//if you want to lock screen for always Portrait mode  
//		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//
//		pb = (ProgressBar) findViewById(R.id.progressBar1);
//		pb.setVisibility(View.INVISIBLE);
//		
//		editLocation = (EditText) findViewById(R.id.editTextLocation);	
//
//		btnGetLocation = (Button) findViewById(R.id.btnLocation);
//		btnGetLocation.setOnClickListener(this);
//		
//		locationMangaer = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//		
//	}

//	@Override
//	public void onClick(View v) {
//		flag = displayGpsStatus();
//		if (flag) {
//			
//			Log.v(TAG, "onClick");		
//			Handler handle = new Handler();
//			handle.postDelayed(new Runnable() {
//
//
//				public void run() {
//					
//				
//	
//			
//			editLocation.setText("Please!! move your device to see the changes in coordinates."+"\nWait..");
//			
//			pb.setVisibility(View.VISIBLE);
//			locationListener = new MyLocationListener();
//
//			locationMangaer.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10,
//	                locationListener);
//				}
//			},12000);
//			status = true;
//			
//			} else {
//			alertbox("Gps Status!!", "Your GPS is: OFF");
//		}
//
//	}
	
	public void getLocation(Context context){
		locationMangaer = (LocationManager) ServiceUberGuest.serviceContext.getSystemService(Context.LOCATION_SERVICE);
		flag = displayGpsStatus(context);
		if (flag) {
			
//			Log.v(TAG, "onClick");		
//			Handler handle = new Handler();
//			handle.postDelayed(new Runnable() {
//
//
//				public void run() {
					
				
	
			
//			editLocation.setText("Please!! move your device to see the changes in coordinates."+"\nWait..");
			
//			pb.setVisibility(View.VISIBLE);
			locationListener = new MyLocationListener();
			

			locationMangaer.requestLocationUpdates(LocationManager.GPS_PROVIDER,15000 , 0,
	                locationListener);
//			locationMangaer.removeUpdates(locationListener);
//				}
//			},12000);
			status = true;
			
			} else {
//			alertbox("Gps Status!!", "Your GPS is: OFF");
//				Toast.makeText(MyService.serviceContext, "Gps Status!!  Your GPS is: OFF", Toast.LENGTH_LONG).show();
		}

	}

	/*----------Method to Check GPS is enable or disable ------------- */
	 public Boolean displayGpsStatus(Context context) {
		ContentResolver contentResolver = ServiceUberGuest.serviceContext.getContentResolver();
		boolean gpsStatus = Settings.Secure.isLocationProviderEnabled(
				contentResolver, LocationManager.GPS_PROVIDER);
		if (gpsStatus) {
			return true;

		} else {
			return false;
		}
	}

	/*----------Method to create an AlertBox ------------- */
//	protected void alertbox(String title, String mymessage) {
//		AlertDialog.Builder builder = new AlertDialog.Builder(this);
//		builder.setMessage("Your Device's GPS is Disable")
//				.setCancelable(false)
//				.setTitle("** Gps Status **")
//				.setPositiveButton("Gps On",
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog, int id) {
//								// finish the current activity
//								// AlertBoxAdvance.this.finish();
//								Intent myIntent = new Intent(
//										Settings.ACTION_SECURITY_SETTINGS);
//								startActivity(myIntent);
//								dialog.cancel();
//							}
//						})
//				.setNegativeButton("Cancel",
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog, int id) {
//								// cancel the dialog box
//								dialog.cancel();
//							}
//						});
//		AlertDialog alert = builder.create();
//		alert.show();
//	}
	
	/*----------Listener class to get coordinates ------------- */
	private class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location loc) {
          
//            	editLocation.setText("");
//            	pb.setVisibility(View.INVISIBLE);
//                Toast.makeText(getBaseContext(),"Location changed : Lat: " + loc.getLatitude()
//                                + " Lng: " + loc.getLongitude(),Toast.LENGTH_SHORT).show();
                longitude = "Longitude:" +loc.getLongitude();  
    			Log.v(TAG, longitude);
    		    latitude = "Latitude:" +loc.getLatitude();
    		    Log.v(TAG, latitude);
    		    
    		    /*----------to get City-Name from coordinates ------------- */
//    		    String cityName=null;      		      
//    		    Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());      		     
//    		    List<Address>  addresses;  
//    		    try {  
//    		     addresses = gcd.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);  
//    		     if (addresses.size() > 0)  
//    		      System.out.println(addresses.get(0).getLocality());  
//    		     cityName=addresses.get(0).getLocality();  
//    		    } catch (IOException e) {    		      
//    		     e.printStackTrace();  
//    		    } 
    		    if(status)
    		    	functionToUpdateStatus();
    		    
        }
        void functionToUpdateStatus(){
        	String Longitude = longitude;
        	String Latitude = latitude;
        	String s = Longitude+"\n"+Latitude ;
        	if (!Longitude.equals("")&&!Latitude.equals("")){
// 		    editLocation.setText(s);
	 		    String[] strLongitude = Longitude.split(":");
	 		    String[] strLatitude = Latitude.split(":");
				List<NameValuePair> userInfo = new ArrayList<NameValuePair>();
				long dtMili = System.currentTimeMillis();
				SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ServiceUberGuest.serviceContext);
					userInfo.add(new BasicNameValuePair("api_key",settings.getString("key", null).toString()));
					userInfo.add(new BasicNameValuePair("time_stamp",String.valueOf(dtMili)));
					userInfo.add(new BasicNameValuePair("longitude",String.valueOf(strLongitude[1])));
					userInfo.add(new BasicNameValuePair("latitude",String.valueOf(strLatitude[1])));
					JSONArray jArray;
					try {
						jArray = new JsonFetcher("http://uberguest.bugs3.com/uberguest/index.php/api/guest_update_location"
								, "GET", userInfo).execute().get();		
					
							JSONObject jobject = jArray.getJSONObject(0);
							int status=jobject.getInt("status");
							if (status ==1){
	//							editLocation.setText(s+"\n status updated in database.");
//								Toast.makeText(MyService.serviceContext, "status updated in database.", Toast.LENGTH_SHORT).show();
							}
							
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				status = false;
        	}
        }

        @Override
        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub        	
        }

        @Override
        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub        	
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // TODO Auto-generated method stub        	
        }
    }
	

 }