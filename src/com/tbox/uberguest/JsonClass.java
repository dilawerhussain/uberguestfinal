package com.tbox.uberguest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class JsonClass {
	
	static InputStream is = null;
    static JSONObject jObj = null;
    static JSONArray jArr=null;
    static String json = "";
    static String result = "";



    // function get json from url
    // by making HTTP POST or GET method
    public static JSONArray makeHttpRequest(String url, String method,
            List<NameValuePair> params) {

        // Making HTTP request
        try {
            // check for request method
            if(method.equals("POST")){
                // request method is POST
                // defaultHttpClient
           	  JSONObject json = new JSONObject();
//               try {
               for(int i=0;i<params.size();i++){
                    
             	 
					try {
						json.put(params.get(i).getName(), params.get(i).getValue());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
						Log.d(params.get(i).getName(),params.get(i).getValue());	
						}
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                List<NameValuePair> postParams = new ArrayList<NameValuePair>();
                postParams.add(new BasicNameValuePair("data",json.toString()));
                //StringEntity se = new StringEntity( json.toString());
               // se.setContentType("application/json;charset=UTF-8");
              //  httpPost.set("data",json.toString());
                UrlEncodedFormEntity entity=new UrlEncodedFormEntity(postParams);
                //httpPost.getParams().setParameter("data","data="+json.toString());
                httpPost.setEntity(entity);
                
               // httpClient.execute(httpPost);
//                JSONObject json = new JSONObject();
//                try {
//                for(int i=0;i<params.size();i++){
//                      
//               	 
//						json.put(params.get(i).getName(), params.get(i).getValue());
//						Log.d(params.get(i).getName(),params.get(i).getValue());
//					}
                
               /// List<NameValuePair> postParams = new ArrayList<NameValuePair>();
               //.add(new BasicNameValuePair("data", params));
                
                
//                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(postParams);
//                entity.setContentEncoding(HTTP.UTF_8);
//                entity.setContentType("application/json");
//                httpPost.setEntity(entity);
//
//                httpPost.setHeader("Content-Type", "application/json");
//                httpPost.setHeader("Accept", "application/json"); 
//                HttpResponse httpResponse = httpClient.execute(httpPost);
//                HttpEntity httpEntity = httpResponse.getEntity();

              
                
//                StringEntity se = new StringEntity( "data="+json.toString());
//                   Log.d("check", json.toString());
//                //  se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
//                se.setContentType("application/json;charset=UTF-8");
//                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
//
//                httpPost.setEntity(se);
                HttpResponse response = httpClient.execute(httpPost);
                
                 is = response.getEntity().getContent();
                //BufferedInputStream bis = new BufferedInputStream(is);
               // ByteArrayBuffer baf = new ByteArrayBuffer(20);
     
               // int current = 0;
                 
              //  while((current = bis.read()) != -1){
              //      baf.append((byte)current);
              //  }  
     
                /* Convert the Bytes read to a String. */
              //  String text = new String(baf.toByteArray());
              //  String str=text;
                
                
                
//                HttpEntity httpEntity = httpResponse.getEntity();
               // is = httpEntity.getContent();
//               
//                } catch (JSONException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//             
                                
                

            }else if(method.equals("GET")){
                // request method is GET
                DefaultHttpClient httpClient = new DefaultHttpClient();
                String paramString = URLEncodedUtils.format(params, "utf-8");
                url += "?" + paramString;
                HttpGet httpGet = new HttpGet(url);

                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            }           

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        // try parse the string to a JSON object
        try {
            jArr = new JSONArray("["+json+"]");
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
        // return JSON String (Array)
        return jArr;
    }


}
