package com.tbox.uberguest;

import java.io.UnsupportedEncodingException;

import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

public class RetreivePasswordActivity extends Activity {
	
Button btn_continue,btn_continue_security_question;
ProgressDialog pDialog;
EditText et_username_email,et_firstname,et_lastname,et_answer;
String email;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_retreive_password);

		et_username_email = (EditText) findViewById(R.id.et_username_email);
		et_firstname = (EditText) findViewById(R.id.et_firstname);
		et_lastname = (EditText) findViewById(R.id.et_lastname);
		btn_continue = (Button) findViewById(R.id.btn_to_retreive_password);
		
		btn_continue.setOnClickListener(
		           new View.OnClickListener() {
		               @Override
		               public void onClick(View v) {
		            	   btn_continue.setEnabled(false);
		            	    email = et_username_email.getText().toString();
		            	   String firstname = et_firstname.getText().toString();
		            	   String lastname = et_lastname.getText().toString();
		            	   if(email.equals("") && firstname.equals("") && lastname.equals(""))
		            	   {
		            		   btn_continue.setEnabled(true);
		            		   Utility.showAlertDialog("please fill all Fields", "Empty Fields", RetreivePasswordActivity.this);
		            		   
		            	   }
		            	   else
		            	   {
		            		   btn_continue.setEnabled(true);
		            		    if(email.equals(""))
		            			   {
		            				   Utility.showAlertDialog("please provide a valid Email", "Email is empty", RetreivePasswordActivity.this);
		            			   }
			            		   else if(firstname.equals(""))
				            	   {
				            		   Utility.showAlertDialog("please provide a Firstname", "Firstname is empty", RetreivePasswordActivity.this);
				            		}
			            		   else if(lastname.equals(""))
					            	{
					            	   Utility.showAlertDialog("please provide a Lastname", "Lastname is empty", RetreivePasswordActivity.this);
					            	 }
			            		   else
			            		   {
				            		   if(Utility.isValidEmail(email))
				            		   {
				            			  makeRequestIsUserExists(email, firstname, lastname); 
				            			}
				            		   else
				            		   {
				            			   Utility.showAlertDialog("please provide a valid Email", "Email is invalid", RetreivePasswordActivity.this);
				            		   }
			            		   }
		            	   }
		               }
		           }
		       );
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		finish();
	}
public void dialogAnswerDidnotMatch(String check_email)
	{
	btn_continue.setEnabled(true);
		new AlertDialog.Builder(RetreivePasswordActivity.this)
		 .setMessage(check_email)
		 .setPositiveButton("ok", new DialogInterface.OnClickListener() {
			 public void onClick(DialogInterface dialog, int which) {
			
			 }
		 	})
		 	.show();
	}
public void dialogCheckYourEmail(String message)
{
	btn_continue.setEnabled(true);
	new AlertDialog.Builder(RetreivePasswordActivity.this)
	 .setMessage(message)
	 .setTitle("Check Your Email")
	 .setPositiveButton("Back to Login", new DialogInterface.OnClickListener() {
		 public void onClick(DialogInterface dialog, int which) {
		finish();
		 }
	 	})
	 	.show();
}

public void makeRequestIsUserExists(final String email,String first_name,String last_name)
{
	pDialog = new ProgressDialog(this);
	pDialog.setMessage("Loading...");
	pDialog.show();
	pDialog.setCancelable(false);
	JSONObject json = new JSONObject();
	try {
		json.put(Constants.kemail_id, email);
		json.put(Constants.kfirst_name, first_name);
		json.put(Constants.klast_name, last_name);
	
	
	RequestParams params = new RequestParams();
    params.put("data", json.toString());
	AsyncHttpClient client = new AsyncHttpClient();
	client.post(Constants.api_base_url+Constants.is_user_exists, params,
			new JsonHttpResponseHandler() {
	
				@Override
				public void onSuccess(int statusCode, JSONObject response) {
					int status;
					try {
	
						Log.e("retreive password res[ponse", response.toString()+ "");
						status = response.getInt(Constants.status);
						Log.e("status", "" + status);
						if(status == 1)
						{
							if(response.getString("success_message").equals("User Exist"))
							{
		            			final Dialog dialog = new Dialog(RetreivePasswordActivity.this);
	            		    	dialog.setContentView(R.layout.dialogue_security_question);
	            		    	dialog.setTitle("Security Question");
	            		    	btn_continue_security_question = (Button)dialog.findViewById(R.id.btn_continue);
	            				et_answer = (EditText)dialog.findViewById(R.id.et_answer);
	            				
	            				dialog.setCancelable(true);
	            		    	btn_continue_security_question.setOnClickListener(new View.OnClickListener() {
	            			     	   @Override
	            			     	   public void onClick(View arg0) {
	            			     	    // TODO Auto-generated method stub
	            			     		  btn_continue.setEnabled(true);
	            			     		   //dialog.dismiss();
	            			     		   if(!et_answer.getText().toString().equals(""))
	            			     		   {
	            			     			   makeRequestValidate_security_answer(email, et_answer.getText().toString());
	            			     			   
	            			     		   }
	            			     		   else
	            			     		   {
	            			     			  btn_continue.setEnabled(true);
	            			     			   Utility.showAlertDialog("Answer is required", "Required Answer", RetreivePasswordActivity.this);
	            			     		   }
	            			     	   } });
	            		    	dialog.show();
							}
						}
						else
						{
							if(response.getString("error_message").equals("User does not exist or First Name or Last Name does not match"))
							{
								Utility.showAlertDialog("User does not exist or First Name or Last Name does not match", "Email and Name Missmatch", RetreivePasswordActivity.this);
							}
							
						}
						
						pDialog.dismiss();
	
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						pDialog.hide();
					}
					
				}
	
				@Override
				protected Object parseResponse(String arg0)
						throws JSONException {
					// TODO Auto-generated method stub
					Log.e("responce", arg0);
					pDialog.dismiss();
					return super.parseResponse(arg0);
				}
	
				@Override
				public void onFailure(Throwable arg0, JSONObject responce) {
					// TODO Auto-generated method stub
					try {
						pDialog.dismiss();
					} catch (Exception e) {
					}
					// super.onFailure(arg0, responce);
					pDialog.dismiss();
				}
			});
	} catch (JSONException e) {
		e.printStackTrace();
	}
			
}

public void makeRequestValidate_security_answer(String email,String security_answer)
{
	pDialog = new ProgressDialog(this);
	pDialog.setMessage("Loading...");
	pDialog.show();
	pDialog.setCancelable(false);
	JSONObject json = new JSONObject();
	try {
		json.put(Constants.kemail_id, email);
		json.put(Constants.ksecurity_answer, security_answer);
	
	
	RequestParams params = new RequestParams();
    params.put("data", json.toString());
	AsyncHttpClient client = new AsyncHttpClient();
	client.post(Constants.api_base_url+Constants.validate_security_answer, params,
			new JsonHttpResponseHandler() {
	
				@Override
				public void onSuccess(int statusCode, JSONObject response) {
					int status;
					try {
	
						Log.e("retreive password res[ponse", response.toString()+ "");
						status = response.getInt(Constants.status);
						Log.e("status", "" + status);
						if(status == 1)
						{
							if(response.getString("success_message").equals("Please check your email"))
							{
								dialogCheckYourEmail("An Email has just been sent to address. Please check your inbox to retrieve your password");
							}
							
						}
						else if(status == 0)
						{
							if(response.getString("error_message").equals("Given answer does not match with actual answer"))
							{
								new AlertDialog.Builder(RetreivePasswordActivity.this)
								 .setMessage(response.getString("error_message"))
								 .setPositiveButton("ok", new DialogInterface.OnClickListener() {
									 public void onClick(DialogInterface dialog, int which) {
									
									 }
								 	})
								 	.show();
//								dialogAnswerDidnotMatch(response.getString("error_message"));
							}
						}
		            			
						
						pDialog.dismiss();
	
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						pDialog.dismiss();
					}
					
				}
	
				@Override
				protected Object parseResponse(String arg0)
						throws JSONException {
					// TODO Auto-generated method stub
					Log.e("responce", arg0);
					pDialog.dismiss();
					return super.parseResponse(arg0);
				}
	
				@Override
				public void onFailure(Throwable arg0, JSONObject responce) {
					// TODO Auto-generated method stub
					pDialog.dismiss();
					try {
						pDialog.dismiss();
					} catch (Exception e) {
					}
					// super.onFailure(arg0, responce);
					
				}
			});
	} catch (JSONException e) {
		e.printStackTrace();
		pDialog.dismiss();
	}
			
}

}
