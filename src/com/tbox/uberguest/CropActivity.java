package com.tbox.uberguest;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.edmodo.cropper.CropImageView;

@SuppressLint("SdCardPath")
public class CropActivity extends Activity{

	static Activity cropactivity;
	DisplayMetrics dimension;
	private Matrix mMatrix = new Matrix();
	CropImageView cropImageView;
	Bitmap croppedImage;

	// Constants
	public static final int MEDIA_GALLERY = 1;
	public static final int TEMPLATE_SELECTION = 2;
	public static final int DISPLAY_IMAGE = 3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_crop);
		
		 String root = Environment.getExternalStorageDirectory().toString();
		    File myDir = new File(root+"/UberGuest");
		    if(!myDir.exists())
			   myDir.mkdirs();
		
		dimension = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dimension);
		
		cropactivity=this;
		Log.e("path of the picture", Constants.image_name.toString());
		cropImageView = (CropImageView)findViewById(R.id.CropImageView);
		
		
        ExifInterface exif=null;
		try {
			exif = new ExifInterface(Constants.image_name.getAbsolutePath());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;

        Bitmap bmp=null;
		try {
			FileInputStream _fis = new FileInputStream(Constants.image_name);
			bmp = BitmapFactory.decodeStream(_fis,null, options);
			_fis.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
        int orientation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL);

        int angle = 0;
        if(Constants.temp_image_gallery == 1)
        {
        	if (orientation == ExifInterface.ORIENTATION_ROTATE_90)
        	{
                angle = 90;
            } 
        	else if (orientation == ExifInterface.ORIENTATION_ROTATE_180)
        	{
                angle = 180;
            } 
        	else if (orientation == ExifInterface.ORIENTATION_ROTATE_270)
        	{
                angle = 270;
            }
        	
        }
        else{

        	if(CustomCameraActivity.is_back_camera == false){
        		if(CustomCameraActivity.snap_orientation_portrait == true){
        			if (orientation == ExifInterface.ORIENTATION_NORMAL) {
                        angle = -90;
                    }
        		}else{
        			if (orientation == ExifInterface.ORIENTATION_NORMAL) {
                        angle = -90;
                    }
        			
        		}
        		 
        	}else{
        		if(CustomCameraActivity.snap_orientation_portrait == true){
        			if (orientation == ExifInterface.ORIENTATION_NORMAL) {
                        angle = 90;
                    }
        		}else{
        			if (orientation == ExifInterface.ORIENTATION_NORMAL) {
                        angle = -90;
                    }	
        		}
        	}
        	 
        }
        

        Matrix mat = new Matrix();
        mat.postRotate(angle);
        Bitmap bitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
        bmp.getHeight(), mat, true);
        ByteArrayOutputStream outstudentstreamOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outstudentstreamOutputStream);
        if(Constants.temp_image_gallery == 1)
        {
        	int screenWidth = dimension.widthPixels ;
        	float temp1 = (screenWidth * bitmap.getHeight())/bitmap.getWidth();
		    int temp2 = Math.round(temp1);
		    bitmap = Bitmap.createScaledBitmap(bitmap, screenWidth, temp2, true);
        }
        cropImageView.setImageBitmap(bitmap);
		
//		ExifInterface exif = null;
//		try {
//			exif = new ExifInterface(Constants.image_name.getAbsolutePath());
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		Bitmap myBitmap = BitmapFactory.decodeFile(Constants.image_name.getAbsolutePath());
//		Bitmap resizedBitmap = Bitmap.createScaledBitmap(myBitmap,
//				myBitmap.getWidth() / 2, myBitmap.getHeight() / 2, false);
//		int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
//		
//		Matrix matrix = new Matrix();
//		if(ExifInterface.ORIENTATION_ROTATE_90 == orientation){
//			matrix.postRotate(90);
//		}
////		else if(ExifInterface.ORIENTATION_ROTATE_270== orientation){
////			matrix.postRotate(-90);
////		}else if(ExifInterface.ORIENTATION_ROTATE_180== orientation){
////			matrix.postRotate(-180);
////		}
//		
//		Bitmap rotatedBitmap = Bitmap.createBitmap(resizedBitmap, 0, 0, resizedBitmap.getWidth(), resizedBitmap.getHeight(), matrix, true);
//		
//		
//		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
////		Bitmap resizedBitmap = Bitmap.createScaledBitmap(myBitmap,
////				myBitmap.getWidth() / 2, myBitmap.getHeight() / 2, false);
////		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
////		Matrix matrix = new Matrix();
////		if (CustomCameraActivity.is_back_camera) {
//////			matrix.postRotate(90);
////		} else {
////			matrix.postRotate(-90);
////		}
////		myBitmap = Bitmap.createBitmap(resizedBitmap, 0, 0,
////				resizedBitmap.getWidth(), resizedBitmap.getHeight(), matrix,
////				true);
////		DisplayMetrics metrics = new DisplayMetrics();
////		getWindowManager().getDefaultDisplay().getMetrics(metrics);
//		rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

//		cropImageView.setImageBitmap(rotatedBitmap);
		//mImg.setScaleType(ScaleType.FIT_XY);
		// CustomCameraActivity.is_back_camera = true;
//		mImg.setImageBitmap(myBitmap);
		
		
		
		cropImageView.setGuidelines(0);
		// Sets initial aspect ratio to 10/10, for demonstration purposes
		cropImageView.setFixedAspectRatio(true);
        cropImageView.setAspectRatio(100, 100);
		
//		// mSelectedVersion =
//		// getIntent().getExtras().getInt(MainActivity.CROP_VERSION_SELECTED_KEY,
//		// -1);
//		mSelectedVersion = 1;
////		mImg = (ImageView) findViewById(R.id.cp_img);
////		mTemplateImg = (ImageView) findViewById(R.id.cp_face_template);
////		mImg.setOnTouchListener(this);
//
//		// Get screen size in pixels.
//		DisplayMetrics metrics = new DisplayMetrics();
//		getWindowManager().getDefaultDisplay().getMetrics(metrics);
//		mScreenHeight = metrics.heightPixels;
//		mScreenWidth = metrics.widthPixels;
//
//		Bitmap faceTemplate = BitmapFactory.decodeResource(getResources(),R.drawable.face_square);
//		mTemplateWidth = faceTemplate.getWidth();
//		mTemplateHeight = faceTemplate.getHeight();
//
//		// Set template image accordingly to device screen size.
//		//if (mScreenWidth == 320 && mScreenHeight == 480) {
//			mTemplateWidth = mScreenWidth;
//			mTemplateHeight = mScreenWidth;
//			faceTemplate = Bitmap.createScaledBitmap(faceTemplate,mTemplateWidth, mTemplateHeight, true);
//			mTemplateImg.setImageBitmap(faceTemplate);
//		//}
//
//		// Load temp image.
//
//		// Bitmap photoImg = BitmapFactory.decodeResource(getResources(),
//		// R.drawable.temp_image);
//
//		Bitmap myBitmap = BitmapFactory.decodeFile(Constants.image_name
//				.getAbsolutePath());
//		Bitmap resizedBitmap = Bitmap.createScaledBitmap(myBitmap,
//				myBitmap.getWidth() / 2, myBitmap.getHeight() / 2, false);
//		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//		Matrix matrix = new Matrix();
//		if (CustomCameraActivity.is_back_camera) {
//			matrix.postRotate(90);
//		} else {
//			matrix.postRotate(-90);
//		}
//		myBitmap = Bitmap.createBitmap(resizedBitmap, 0, 0,
//				resizedBitmap.getWidth(), resizedBitmap.getHeight(), matrix,
//				true);
//		
//		myBitmap = Bitmap.createScaledBitmap(myBitmap, mScreenWidth+100, mScreenHeight+30, true);
//		
//		myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
//
//		//mImg.setScaleType(ScaleType.FIT_XY);
//		// CustomCameraActivity.is_back_camera = true;
//		mImg.setImageBitmap(myBitmap);
//		
//		//mImg.getLayoutParams().width = myBitmap.getHeight();
//		//mImg.getLayoutParams().height = myBitmap.getWidth();
//		// mImg.setImageBitmap(photoImg);
//		mImageHeight = myBitmap.getHeight();
//		mImageWidth = myBitmap.getWidth();
//
//		// View is scaled by matrix, so scale initially
//		mMatrix.postScale(mScaleFactor, mScaleFactor);
//		mImg.setImageMatrix(mMatrix);
//
//		// Setup Gesture Detectors
//		mScaleDetector = new ScaleGestureDetector(getApplicationContext(),
//				new ScaleListener());
//		mRotateDetector = new RotateGestureDetector(getApplicationContext(),
//				new RotateListener());
//		mMoveDetector = new MoveGestureDetector(getApplicationContext(),
//				new MoveListener());
//
//		// Instantiate Thread Handler.
//		mCropHandler = new CropHandler(this);
	}

	public void onCropImageButton(View v) {
		
		croppedImage = cropImageView.getCroppedImage();
		
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		croppedImage.compress(Bitmap.CompressFormat.JPEG,100, bytes);
		
		String filePath = "/UberGuest/profile_image.jpg";
        File f = new File(Environment.getExternalStorageDirectory().getPath(), filePath);
		try {
			if(f.exists())
				f.delete();
			
			f.createNewFile();
			FileOutputStream fo = new FileOutputStream(f);
			fo.write(bytes.toByteArray());
			fo.close();
			CustomCameraActivity.is_back_camera = true;
			Utility.profile_image_path = f.getAbsolutePath().toString();
			Utility.isImageCaptured=true;
		} catch (Exception E) {
		}
		new AlertDialog.Builder(cropactivity)
	    .setTitle("")
	    .setMessage(R.string.msg_picture_done)
	    .setPositiveButton(R.string.alert_ok, new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	        	cropactivity.finish();
	        }
	     })
	     .show();
		
		
		
//        ImageView croppedImageView = (ImageView) findViewById(R.id.croppedImageView);
//        croppedImageView.setImageBitmap(croppedImage);
		
//		// Create progress dialog and display it.
//		mProgressDialog = new ProgressDialog(v.getContext());
//		mProgressDialog.setCancelable(false);
//		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//		mProgressDialog.setMessage("Cropping Image\nPlease Wait.....");
//		mProgressDialog.show();
//
//		// Setting values so that we can retrive the image from
//		// ImageView multiple times.
//		mImg.buildDrawingCache(true);
//		mImg.setDrawingCacheEnabled(true);
//		mTemplateImg.buildDrawingCache(true);
//		mTemplateImg.setDrawingCacheEnabled(true);
//
//		// Create new thread to crop.
//		new Thread(new Runnable() {
//			@Override
//			public void run() {
//				// Crop image using the correct template size.
//				Bitmap croppedImg = null;
//				if (mScreenWidth == 320 && mScreenHeight == 480) {
//					if (mSelectedVersion == 1) {
//						croppedImg = ImageProcess.cropImage(
//								mImg.getDrawingCache(true),
//								mTemplateImg.getDrawingCache(true),
//								mTemplateWidth, mTemplateHeight,getApplicationContext());
//					} else {
//						croppedImg = ImageProcess.cropImageVer2(
//								mImg.getDrawingCache(true),
//								mTemplateImg.getDrawingCache(true),
//								mTemplateWidth, mTemplateHeight);
//					}
//				} else {
//					if (mSelectedVersion == 1) {
//						croppedImg = ImageProcess.cropImage(
//								mImg.getDrawingCache(true),
//								mTemplateImg.getDrawingCache(true),
//								mTemplateWidth, mTemplateHeight,getApplicationContext());
//
//					} else {
//						croppedImg = ImageProcess.cropImageVer2(
//								mImg.getDrawingCache(true),
//								mTemplateImg.getDrawingCache(true),
//								mTemplateWidth, mTemplateHeight);
//					}
//				}
//				mImg.setDrawingCacheEnabled(false);
//				mTemplateImg.setDrawingCacheEnabled(false);
//
//				// Send a message to the Handler indicating the Thread has
//				// finished.
//				mCropHandler.obtainMessage(DISPLAY_IMAGE, -1, -1, croppedImg)
//						.sendToTarget();
//			}
//		}).start();
	}

	// public void onChangeTemplateButton(View v) {
	// Intent intent = new Intent(this, TemplateSelectDialog.class);
	// startActivityForResult(intent, TEMPLATE_SELECTION);
	// }

	// public void onChangeImageButton(View v) {
	// // Start Gallery App.
	// Intent intent = new Intent(Intent.ACTION_PICK,
	// android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	// startActivityForResult(intent, MEDIA_GALLERY);
	// }

//	/*
//	 * Adjust the size of bitmap before loading it to memory. This will help the
//	 * phone by not taking up a lot memory.
//	 */
//	private void setSelectedImage(String path) {
//		final BitmapFactory.Options options = new BitmapFactory.Options();
//		options.inJustDecodeBounds = true;
//		BitmapFactory.decodeFile(path, options);
//		if (mScreenWidth == 320 && mScreenHeight == 480) {
//			options.inSampleSize = calculateImageSize(options,
//					IMG_MAX_SIZE_MDPI);
//		} else {
//			options.inSampleSize = calculateImageSize(options, IMG_MAX_SIZE);
//		}
//
//		options.inJustDecodeBounds = false;
//		Bitmap photoImg = BitmapFactory.decodeFile(path, options);
//		mImageHeight = photoImg.getHeight();
//		mImageWidth = photoImg.getWidth();
//		mImg.setImageBitmap(photoImg);
//	}

//	/*
//	 * Retrieves the path to the selected image from the Gallery app.
//	 */
//	private String getGalleryImagePath(Intent data) {
//		Uri imgUri = data.getData();
//		String filePath = "";
//		if (data.getType() == null) {
//			// For getting images from gallery.
//			String[] filePathColumn = { MediaStore.Images.Media.DATA };
//			Cursor cursor = getContentResolver().query(imgUri, filePathColumn,
//					null, null, null);
//			cursor.moveToFirst();
//			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//			filePath = cursor.getString(columnIndex);
//			cursor.close();
//		}
//		return filePath;
//	}
//
//	/*
//	 * Calculation used to determine by what factor images need to be reduced
//	 * by. Images with its longest side below the threshold will not be resized.
//	 */
//	private int calculateImageSize(BitmapFactory.Options opts, int threshold) {
//		int scaleFactor = 1;
//		final int height = opts.outHeight;
//		final int width = opts.outWidth;
//
//		if (width >= height) {
//			scaleFactor = Math.round((float) width / threshold);
//		} else {
//			scaleFactor = Math.round((float) height / threshold);
//		}
//		return scaleFactor;
//	}

	// @Override
	// protected void onActivityResult(int requestCode, int resultCode, Intent
	// data) {
	// super.onActivityResult(requestCode, resultCode, data);
	//
	// if (resultCode == RESULT_OK) {
	// if (requestCode == MEDIA_GALLERY) {
	// String path = getGalleryImagePath(data);
	//
	// setSelectedImage(path);
	// } else if (requestCode == TEMPLATE_SELECTION) {
	// int pos = data.getExtras().getInt(TemplateSelectDialog.POSITION);
	// Bitmap templateImg = null;
	//
	// // Change template according to what the user has selected.
	// switch(pos) {
	// case 0:
	// templateImg = BitmapFactory.decodeResource(getResources(),
	// R.drawable.face_oblong);
	// break;
	// case 1:
	// templateImg = BitmapFactory.decodeResource(getResources(),
	// R.drawable.face_oval);
	// break;
	// case 2:
	// templateImg = BitmapFactory.decodeResource(getResources(),
	// R.drawable.face_round);
	// break;
	// case 3:
	// templateImg = BitmapFactory.decodeResource(getResources(),
	// R.drawable.face_square);
	// break;
	// case 4:
	// templateImg = BitmapFactory.decodeResource(getResources(),
	// R.drawable.face_triangular);
	// break;
	// }
	//
	// mTemplateWidth = templateImg.getWidth();
	// mTemplateHeight = templateImg.getHeight();
	//
	// // Resize template if necessary.
	// if (mScreenWidth == 320 && mScreenHeight == 480) {
	// mTemplateWidth = 218;
	// mTemplateHeight = 300;
	// templateImg = Bitmap.createScaledBitmap(templateImg, mTemplateWidth,
	// mTemplateHeight, true);
	// }
	// mTemplateImg.setImageBitmap(templateImg);
	// }
	// }
	// }

//	private static class CropHandler extends Handler {
//		WeakReference<CropActivity> mThisCA;
//
//		CropHandler(CropActivity ca) {
//			mThisCA = new WeakReference<CropActivity>(ca);
//		}
//
//		@SuppressLint("SdCardPath")
//		@Override
//		public void handleMessage(Message msg) {
//			super.handleMessage(msg);
//
//			CropActivity ca = mThisCA.get();
//			if (msg.what == DISPLAY_IMAGE) {
//				mProgressDialog.dismiss();
//
//				final Bitmap cropImg = (Bitmap) msg.obj;
//
//				// Setup an AlertDialog to display cropped image.
////				AlertDialog.Builder builder = new AlertDialog.Builder(ca);
////				builder.setTitle("Final Cropped Image");
////				builder.setIcon(new BitmapDrawable(ca.getResources(), cropImg));
////				builder.setPositiveButton("OK",
////						new DialogInterface.OnClickListener() {
////							public void onClick(DialogInterface dialog, int id) {
//
//								Bitmap myBitmap = cropImg;
//								Bitmap resizedBitmap = Bitmap.createScaledBitmap(
//										myBitmap, myBitmap.getWidth() / 3,
//										myBitmap.getHeight() / 3, false);
//								
//								ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//
//								Matrix matrix = new Matrix();
//								
//								if (CustomCameraActivity.is_back_camera)
//								{
//									//matrix.postRotate(90);
//								}
//								else 
//								{
//									//matrix.postRotate(-90);
//								}
//								myBitmap = Bitmap.createBitmap(resizedBitmap,0, 0, resizedBitmap.getWidth(),resizedBitmap.getHeight(), matrix, true);
//								myBitmap.compress(Bitmap.CompressFormat.JPEG,100, bytes);
//								
//								String filePath = "/UberGuest/profile_image.jpg";
//						        File f = new File("/sdcard", filePath);
//								try {
//									if(f.exists())
//										f.delete();
//									
//									f.createNewFile();
//									FileOutputStream fo = new FileOutputStream(f);
//									fo.write(bytes.toByteArray());
//									fo.close();
//									CustomCameraActivity.is_back_camera = true;
//									Utility.profile_image_path = f.getAbsolutePath().toString();
//									Utility.isImageCaptured=true;
////									Utility.profile_image_path = new File(f.getAbsolutePath().toString());
////									HomeActivity.selectedImage	= Uri.fromFile(new File(f.getAbsolutePath().toString()));
////									SignupActivity.picturePath = f.getAbsolutePath().toString();
////									EditProfileActivity.picturePath = f.getAbsolutePath().toString();
//								} catch (Exception E) {
//								}
//								cropactivity.finish();
//								
////								dialog.cancel();
//								
//							}
////						});
////				AlertDialog dialog = builder.create();
////				dialog.show();
////			}
//		}
//	}

//	@SuppressWarnings("static-access")
//	public boolean onTouch(View v, MotionEvent event) {
//		mScaleDetector.onTouchEvent(event);
//		mRotateDetector.onTouchEvent(event);
//		mMoveDetector.onTouchEvent(event);
//		int action = MotionEventCompat.getActionMasked(event);
//		if(action != event.ACTION_DOWN){
//			float scaledImageCenterX = 0;//(mImageWidth * mScaleFactor) / 2;
//			float scaledImageCenterY = 0;//(mImageHeight * mScaleFactor) / 2;
//
//			mMatrix.reset();
//			mMatrix.postScale(mScaleFactor, mScaleFactor);
//			mMatrix.postRotate(mRotationDegrees, scaledImageCenterX,
//					scaledImageCenterY);
//			mMatrix.postTranslate(mFocusX - scaledImageCenterX, mFocusY
//					- scaledImageCenterY);
//
//			ImageView view = (ImageView) v;
//			view.setImageMatrix(mMatrix);
//
//		}
//		return true;
//	}
//
//	private class ScaleListener extends
//			ScaleGestureDetector.SimpleOnScaleGestureListener {
//		@Override
//		public boolean onScale(ScaleGestureDetector detector) {
//			mScaleFactor *= detector.getScaleFactor();
//			mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 10.0f));
//
//			return true;
//		}
//	}
//
//	private class RotateListener extends
//			RotateGestureDetector.SimpleOnRotateGestureListener {
//		@Override
//		public boolean onRotate(RotateGestureDetector detector) {
//			mRotationDegrees -= detector.getRotationDegreesDelta();
//			return true;
//		}
//	}
//
//	private class MoveListener extends
//			MoveGestureDetector.SimpleOnMoveGestureListener {
//		@Override
//		public boolean onMove(MoveGestureDetector detector) {
//			PointF d = detector.getFocusDelta();
//			mFocusX += d.x;
//			mFocusY += d.y;
//
//			return true;
//		}
//	}
	
	@Override
	public  void onBackPressed()
	{
		Constants.temp_image_gallery = 0;
//		onCropImageButton(findViewById(R.id.btn_crop));
		CropActivity.this.finish();
	}
}
