package com.tbox.uberguest;


import android.content.Intent;

public enum ScreenType {
	Voice_Record_From_Signup,
	Voice_Record_From_EditProfile,
	Home_From_Login,	
	Home_From_Map,
	Home_From_Sign_Up,
	Home_Default;
	
	
	private static final String name = ScreenType.class.getName();
	  public void attachTo(Intent intent) {
	    intent.putExtra(name, ordinal());
	  }
	  public static ScreenType detachFrom(Intent intent) {
	    if(!intent.hasExtra(name)) throw new IllegalStateException();
	    return values()[intent.getIntExtra(name, -1)];
	  }
}
