package com.tbox.uberguest;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Scroller;

import com.android.volley.toolbox.ImageLoader;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.makeramen.RoundedImageView;
import com.tbox.backend.AppController;
import com.tbox.backend.FileUploader;
import com.tbox.backend.FileUploaderInterface;

public class EditProfileActivity extends Activity implements FileUploaderInterface,LocationListener{
	Button 				btn_button_load_image,btn_voice_record;
	EditText 			et_first_name , et_last_name , et_password , et_city , et_country,et_state,et_title,et_like,et_company,
							et_dislike,et_spouse,et_birthday,et_special_instruction,et_cell_number,et_email,et_security_answer;
	Button 				btn_update;
	ProgressDialog 		progressDialog;
	RoundedImageView 	profile_imageview;
	public LocationManager locationMangaer = null;
	private LocationListener locationListener = null;
	String apiKey= "";
	String ime="";
	Bitmap 				bitmap;
	double longitude,latitude;
	Context 			context;
	int 				status = 0;

	private void setPreFillValues(){
		UserInfo userInfo = UserInfo.getInstance(EditProfileActivity.this);
		et_first_name.setText(userInfo.first_name);
		et_last_name.setText(userInfo.last_name);
		Log.e("security answwer", userInfo.security_answer+"--");
		et_security_answer.setText(userInfo.security_answer);
		et_password.setText(userInfo.password);
		et_state.setText(userInfo.state);
		et_city.setText(userInfo.city);
		et_country.setText(userInfo.country);
		et_title.setText(userInfo.title);
		et_email.setText(userInfo.email_id);
		et_like.setText(userInfo.like);
		et_company.setText(userInfo.company);
//		et_spouse.setText(userInfo.spouse);
		et_dislike.setText(userInfo.dislike);
		et_birthday.setText(userInfo.birthday);
		et_special_instruction.setText(userInfo.Special_Instruction);
		et_cell_number.setText(userInfo.cell_number);
		 
		ImageLoader imageLoader = AppController.getInstance().getImageLoader();
        imageLoader.get(userInfo.profile_image_path, ImageLoader.getImageListener(
        		profile_imageview, R.drawable.profile_image, R.drawable.profile_image));
	}
	
	private void registerAllElements() {
		// TODO Auto-generated method stub
	
//		btn_button_load_image = (Button) findViewById(R.id.imageUpLoadEdit);
		btn_update = (Button) findViewById(R.id.registerEdit);
        et_first_name = (EditText)findViewById(R.id.userFirstNameEdit);
        et_last_name = (EditText)findViewById(R.id.userLastNameEdit);
        et_password = (EditText)findViewById(R.id.userPasswordEdit);
        et_security_answer = (EditText)findViewById(R.id.user_security_question);
        et_city = (EditText)findViewById(R.id.user_cityEdit);
        et_state = (EditText)findViewById(R.id.user_stateEdit);
        et_country = (EditText)findViewById(R.id.user_countryEdit);
        et_like 		= (EditText)findViewById(R.id.user_likeEdit);
        et_dislike		= (EditText)findViewById(R.id.user_dislikeEdit);
//        et_spouse		= (EditText)findViewById(R.id.user_spouseEdit);
        et_birthday		= (EditText)findViewById(R.id.user_birthdayEdit);
        et_special_instruction	= (EditText)findViewById(R.id.user_specialInstructionEdit);
        et_cell_number	= (EditText)findViewById(R.id.user_cellNumberEdit);
        et_dislike.setMaxLines(3);
//        et_spouse.setMaxLines(3);
        et_birthday.setMaxLines(3);
        et_like .setScroller(new Scroller(this)); 
        et_like .setMaxLines(3); 
        et_like .setVerticalScrollBarEnabled(true); 
        et_like .setMovementMethod(new ScrollingMovementMethod()); 
        et_special_instruction.setScroller(new Scroller(this)); 
        et_special_instruction.setMaxLines(3); 
        et_special_instruction.setVerticalScrollBarEnabled(true); 
        et_special_instruction.setMovementMethod(new ScrollingMovementMethod()); 
        et_email 	    = (EditText)findViewById(R.id.userEmailEdit);
        et_title 		= (EditText)findViewById(R.id.user_tileEdit);
        et_company		= (EditText)findViewById(R.id.user_companyEdit);
        btn_voice_record = (Button) findViewById(R.id.btnVoiceRecordEdit);
//        pb = (ProgressBar)findViewById(R.id.progressBar1Edit);
        profile_imageview = (RoundedImageView)findViewById(R.id.imageViewEdit);
        
        et_like.setScroller(new Scroller(this)); 
        et_like .setMaxLines(3); 
        
	}
	
	private void attachListeners(){
		profile_imageview.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	Intent intent = new Intent(EditProfileActivity.this,CustomCameraActivity.class);
            	startActivity(intent);
            }
        });
	   	btn_voice_record.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
                 
            	Intent intent_Voice =new Intent(EditProfileActivity.this,VoiceRecordActivity.class);
            	ScreenType.Voice_Record_From_EditProfile.attachTo(intent_Voice);
                startActivity(intent_Voice);
            }
        });
 		btn_update.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	locationMangaer.removeUpdates(locationListener);
            	if(Utility.isDataValid(et_email.getText().toString(),et_password.getText().toString(),et_password.getText().toString(),et_first_name.getText().toString(),et_last_name.getText().toString(),
            			et_security_answer.getText().toString(),EditProfileActivity.this)){
            		// add user data as params
        			JSONObject json = new JSONObject();
        			try {
        				json.put(Constants.kfirst_name,et_first_name.getText().toString());
        				json.put(Constants.klast_name,et_last_name.getText().toString());
        				json.put("password",et_password.getText().toString());
        				json.put(Constants.kcity,et_city.getText().toString());
        				json.put(Constants.kstate,et_state.getText().toString());
        				json.put(Constants.klatitude,Double.toString(latitude));
        				json.put(Constants.klongitude,Double.toString(longitude));
        				json.put(Constants.kemail_id,et_email.getText().toString());
        				json.put(Constants.kcountry,et_country.getText().toString());
        				json.put(Constants.ktitle,et_title.getText().toString());
        				json.put(Constants.klike,et_like.getText().toString());
        				json.put(Constants.kcompany,et_company.getText().toString());
        				json.put(Constants.k_answer,et_security_answer.getText().toString());
        				Log.e("security answer update", et_security_answer.getText().toString()+"--");
        				json.put(Constants.kdislike,et_dislike.getText().toString());
//        				json.put(Constants.kspouse,et_spouse.getText().toString());
        				json.put(Constants.kbirthday,et_birthday.getText().toString());
        				json.put(Constants.kcell_number,et_cell_number.getText().toString());
        				json.put(Constants.kspecial_instruction,et_special_instruction.getText().toString());
        				json.put(Constants.kapi_key, UserInfo.getInstance(EditProfileActivity.this).api_key);
                        json.put("device_id",ime);
        			} catch (JSONException e) {
        				// TODO Auto-generated catch block
        				e.printStackTrace();
        			}
        			RequestParams params = new RequestParams();
        	        params.put("data", json.toString());
        	        //start spinner
        	        progressDialog = ProgressDialog.show(EditProfileActivity.this, "", Utility.getString(EditProfileActivity.this, R.string.edit_profile_waiting));
        	        progressDialog.setCancelable(false);
        	      //send post request for user login
        	    	AsyncHttpClient client = new AsyncHttpClient();
        	    	client.post(Constants.api_base_url+Constants.guest_edit_profile, params , new JsonHttpResponseHandler(){
        	    		@Override
                        public void onSuccess(final JSONObject object){
        	    			int status = 0;
        					try {
        						Log.e("json response", object.toString());
        						status = object.getInt("status");
        					} catch (JSONException e) {
        						// TODO Auto-generated catch block
        						e.printStackTrace();
        					}
        					if(status ==1){    
    		            		UserInfo userInfo = UserInfo.getInstance(EditProfileActivity.this);
			            		userInfo.first_name = et_first_name.getText().toString();
			            		userInfo.last_name = et_last_name.getText().toString();
			            		userInfo.city = et_city.getText().toString();
			            		userInfo.email_id = et_email.getText().toString();
			            		userInfo.state = et_state.getText().toString();
			            		userInfo.latitude = Double.toString(latitude);
			            		userInfo.longitude = Double.toString(longitude);
			            		userInfo.country = et_country.getText().toString();
			            		userInfo.password = et_password.getText().toString();
			            		userInfo.like = et_like.getText().toString();
			            		userInfo.security_answer = et_security_answer.getText().toString();
			            		userInfo.title = et_title.getText().toString();
			            		userInfo.company = et_company.getText().toString();
			            		userInfo.dislike = et_dislike.getText().toString();
//			            		userInfo.spouse = et_spouse.getText().toString();
			            		userInfo.birthday = et_birthday.getText().toString();
			            		userInfo.cell_number = et_cell_number.getText().toString();
			            		userInfo.Special_Instruction = et_special_instruction.getText().toString();
			            		//if neither voice nor image is selected (there might be using previous ones)
			            		if(Utility.isImageCaptured!=true && Utility.isVoiceRecorded != true){
			            			progressDialog.dismiss();
			            			finish();
			            		}
			            		//else
			            		if(Utility.isImageCaptured == true){
			            			new FileUploader(EditProfileActivity.this, Utility.profile_image_path,Constants.api_base_url+Constants.guest_image_upload +userInfo.api_key,"image","image/jpeg",0).execute();
			            		}
			            		if(Utility.isVoiceRecorded == true){
			            			new FileUploader(EditProfileActivity.this, Utility.voice_path,Constants.api_base_url+"upload_guest_voice/"+userInfo.api_key,"voice","audio/mp4",1).execute();
			            		}
        		            }
        					else
        					{
        						Utility.showAlertDialog("your server not responding .....!","Sorry", EditProfileActivity.this);
        					}
        	    		}
        	    	
        	    		@Override
						protected Object parseResponse(String arg0)
								throws JSONException {
							// TODO Auto-generated method stub
							Log.e("Responce", arg0);
							return super.parseResponse(arg0);
						}

						@SuppressLint("InlinedApi")
						@Override
						public void onFailure(Throwable throwable,
								JSONObject jsonObject) {
							
								progressDialog.dismiss();
						}

        	    	});
            	}
            			
            }
            
 		});
		
	}
	
	@SuppressWarnings("static-access")
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    setContentView(R.layout.edit_profile_activity);
	    
	    locationMangaer = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = new SignupActivity();
        locationMangaer.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 0, locationListener);
	    
	    context = this;
	    registerAllElements();
	    setPreFillValues();
	    attachListeners();
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(this.TELEPHONY_SERVICE);
        ime=telephonyManager.getDeviceId();
        Log.e("Unique ID", "IME"+telephonyManager.getDeviceId());
	}

	 public boolean isDataValid(String password,String fisrtName) {
		 	if(fisrtName.length()==0){
		 		Utility.showAlertDialog("your first Name is empty.....!","Firstname is missing",EditProfileActivity.this);
		 		return false;
		 	}else if(password.length()==0){
		 		Utility.showAlertDialog("your password is empty.....!","Password is missing", EditProfileActivity.this);
				return false;
			}else{						
				return true;
					
			}
		}
	 @Override
		public void onPause()
		{
			super.onPause();
			Constants.temp_image_gallery = 0;
			locationMangaer.removeUpdates(locationListener);
		}
	 @Override
		public void onResume()
		{
			super.onResume();
			if(Constants.temp_image_gallery == 1)
			{
				try{
					
					Bitmap myBitmap = BitmapFactory.decodeFile(Utility.profile_image_path);
					profile_imageview.setImageBitmap(myBitmap);
					
					}catch(Exception E)
					{}
			}
			else if(Constants.temp_image_gallery == 2)
			{
				Bitmap myBitmap = BitmapFactory.decodeFile(Utility.profile_image_path);
				CustomCameraActivity.is_back_camera = true;
			    profile_imageview.setImageBitmap(myBitmap);
			}
			
		}

	@Override
	public void fileUploaded(FileUploader fileUploader) {
		// TODO Auto-generated method stub
		if(Utility.isImageCaptured == true){
			Utility.isImageCaptured=false;
			if(Utility.isVoiceRecorded !=true){
				progressDialog.dismiss();
				finish();
			}
		}
		if(Utility.isVoiceRecorded == true){
			Utility.isVoiceRecorded=false;
			if(Utility.isImageCaptured !=true){
				progressDialog.dismiss();
				finish();
			}
		}
		
	}

	@Override
	public float uploadProgress(FileUploader fileUploader,
			int downloadProgress) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		longitude = location.getLongitude();
		latitude = location.getLatitude();
		Log.e("lat long", longitude+"--");
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
	
	 
	
}
