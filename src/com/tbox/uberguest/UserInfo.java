package com.tbox.uberguest;

import com.google.gson.Gson;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class UserInfo {
	private static final int MODE_PRIVATE = 0;
	public String api_key;
	public String first_name;
	public String last_name;
	public String password;
	public String security_answer;
	public String city;
	public String state;
	public String country;
	public String is_password_changed;
	public String title;
	public String reasson;
	public String latitude;
	public String longitude;
	public String like;
	public String dislike;
	public String birthday;
	public String spouse;
	public String company;
	public String cell_number;
	public String Special_Instruction;
	public String profile_image_path;
	public String name_recording_path;
	public String email_id;
	
	public Boolean is_login=false;
	private static UserInfo   _instance;

	    
    public static UserInfo getInstance(Context context)
    {
        if (_instance == null)
        {
            _instance = getUserInfo(context);
        }
        return _instance;
    }
    
    public void saveUserInfo(Context context){
    	this.is_login=true;
    	SharedPreferences prefs = context.getSharedPreferences("userInfo",MODE_PRIVATE);
    	Editor prefsEditor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(this);
        prefsEditor.putString("UserInfo", json);
        prefsEditor.commit();
    }
    public void removeUserInfo(Context context){
    	this.is_login=false;
    	SharedPreferences prefs = context.getSharedPreferences("userInfo",MODE_PRIVATE);
    	this.api_key="";
        this.first_name="";
        this.last_name="";
        this.city="";
        this.country="";
        this.state = "";
        this.like="";
        this.dislike="";
        this.birthday="";
        this.spouse="";
        this.is_password_changed = "";
        this.reasson="";
        this.title="";
        this.security_answer = "";
        this.company="";
        this.profile_image_path="";
        this.name_recording_path="";
        this.Special_Instruction="";
        this.cell_number="";
        Gson gson = new Gson();
        String json = gson.toJson(this);
    	Editor prefsEditor = prefs.edit();
        prefsEditor.putString("UserInfo", json);
        prefsEditor.commit();
    }
    
    private static UserInfo getUserInfo(Context context){
    	  Gson gson = new Gson();
    	  SharedPreferences prefs = context.getSharedPreferences("userInfo",MODE_PRIVATE);
    	  String json = prefs.getString("UserInfo", null);
    	  if(json != null){
    		  UserInfo userInfo = gson.fromJson(json, UserInfo.class);
    		  if(userInfo.api_key.length() >0){
    			  userInfo.is_login=true;
    		  }
    		  return userInfo;
    	        
    	   }else{
    		   return new UserInfo();
    	   }
    }
}
