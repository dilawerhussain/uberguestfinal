package com.tbox.uberguest;

import java.io.FileNotFoundException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

public class Utility {

	static String profile_image_path = "";
	static String voice_path = "";
	static boolean isVoiceRecorded = false; 
	static boolean isImageCaptured = false; 
	static String getString(Context ctx, int strId)
	 {
		 return ctx.getResources().getString(strId);
	 }
	static public void showAlertDialog(String message, String title,Context context)
	 {
		 new AlertDialog.Builder(context)
		 .setMessage(message)
		 .setTitle(title)
		 .setPositiveButton("OK", new DialogInterface.OnClickListener() {
			 public void onClick(DialogInterface dialog, int which) {
			 }
		 	})
		 	.show();
	 }
	public static boolean isValidEmail(CharSequence target) {
	  if (target == null) 
	   return false;
	   else 
	   return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
	  
	}
	public static boolean isDataValid(String email, String password,String confirm_password,String fisrtName,String last_name,String security_answer,Context context) {
 
	 	if(email.length()==0 || security_answer.length()==0 || password.length()==0 || confirm_password.length() == 0 || fisrtName.length() == 0 || last_name.length() == 0)
	 	{
	 		Utility.showAlertDialog("Please Enter the required fields","Empty Fields", context);
			return false;
		}
	 	else{	
			if(!Utility.isValidEmail(email))
			{
				Utility.showAlertDialog("your email id is not correct...!","Wrong Email format", context);
				return false;
			}
			else if(!confirm_password.equals(password))
			{
				Utility.showAlertDialog("your both Passwords does not match..!","Passwords must be same", context);
				return false;
			}
			else
				return true;
			
		}
	}	   
    public static Bitmap decodeUri(Uri selectedImage,Context context) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(context.getContentResolver().openInputStream(selectedImage), null, o);

        final int REQUIRED_SIZE = 100;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(
                context.getContentResolver().openInputStream(selectedImage), null, o2);
    }

    public static boolean isLoginValid(String email, String password,Context context) {
    	 
	 	if(email.length()==0 || password.length()==0 )
	 	{
	 		Utility.showAlertDialog("Please Enter the required fields","Empty Fields", context);
			return false;
		}
	 	else{	
			if(!Utility.isValidEmail(email))
			{
				Utility.showAlertDialog("your email id is not correct...!","Wrong Email format", context);
				return false;
			}
			else
				return true;
			
		}
	}

}
