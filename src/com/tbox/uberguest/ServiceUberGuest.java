package com.tbox.uberguest;

import java.util.List;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class ServiceUberGuest extends Service {
	static public Context serviceContext;
	static boolean NetworkConnection = true;
	public LocationManager locationMangaer = null;
	private LocationListener locationListener = null;
	NotificationManager notificationManager;
	Boolean IsNotificationSent = false;

	int time = 20000;
	String ime;
	String longitude = "", latitude = "";
	private static final String TAG = "Debug";
	List<NameValuePair> userInfo;
	JSONArray jArray;
	Notification n;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressLint({ "InlinedApi", "NewApi" })
	@Override
	public void onCreate() {
		super.onCreate();
		locationListener = new MyLocationListener();
		locationMangaer = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(this.TELEPHONY_SERVICE);
		ime = telephonyManager.getDeviceId();
		Log.e("Unique ID", "IME" + telephonyManager.getDeviceId());
		if (locationMangaer.getAllProviders().contains("gps")) 
		{
			Log.e("GPS", "Device GPS Enabled "
					+ locationMangaer.getAllProviders().contains("gps"));
		}
		else 
		{
			Log.e("GPS", "Device GPS Enabled "
					+ locationMangaer.getAllProviders().contains("gps"));
			Toast.makeText(getApplicationContext(),
					"Sorry! This Device is not GPS Enabled.", Toast.LENGTH_LONG)
					.show();
		}
		serviceContext = this;
		Criteria myCriteria = new Criteria();
		myCriteria.setAccuracy(Criteria.ACCURACY_FINE);
		myCriteria.setPowerRequirement(Criteria.POWER_LOW);
		String myProvider = locationMangaer.getBestProvider(myCriteria, true);
		if (displayGpsStatus(serviceContext))
			locationMangaer.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, time, 0, locationListener);

		notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		
//		Intent intent = new Intent(this, AlarmManagerRecieverToStartService.class);
//		PendingIntent pendingIntent = PendingIntent.getBroadcast(
//				this.getApplicationContext(), 234324243, intent, 0);
//
//		AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
//		alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME, 30000,(3 * 10000), pendingIntent);
//		Toast.makeText(this, "Alarm set in " + 30 + " seconds",Toast.LENGTH_LONG).show();
		// prepare intent which is triggered if the
		// notification is selected

	}

	public static void displayPromptForEnablingGPS(final Context ctx) {

		final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
		final String message = "Do you want open GPS setting?";

		builder.setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface d, int id) {
						ctx.startActivity(new Intent(action));
						// ctx.startActivity();
						d.dismiss();
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface d, int id) {
								d.cancel();
							}
						});
		builder.create().show();
	}

	public void turnGPSOn() {
		Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
		intent.putExtra("enabled", true);
		ServiceUberGuest.this.sendBroadcast(intent);
		String provider = Settings.Secure.getString(
				ServiceUberGuest.this.getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if (!provider.contains("gps")) { // if gps is disabled
			final Intent poke = new Intent();
			poke.setClassName("com.android.settings",
					"com.android.settings.widget.SettingsAppWidgetProvider");
			poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
			poke.setData(Uri.parse("3"));
			ServiceUberGuest.this.sendBroadcast(poke);
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onStart(Intent intent, int startId) {
	}

	@Override
	public void onDestroy() {
		locationMangaer.removeUpdates(locationListener);
		this.stopSelf();
		Log.d("helo", "onDestroy");
	}

	public Boolean displayGpsStatus(Context context) {
		ContentResolver contentResolver = ServiceUberGuest.serviceContext
				.getContentResolver();
		boolean gpsStatus = Settings.Secure.isLocationProviderEnabled(
				contentResolver, LocationManager.GPS_PROVIDER);
		if (gpsStatus) {
			return true;

		} else {
			return false;
		}
	}

	private class MyLocationListener implements LocationListener {

		@Override
		public void onLocationChanged(Location loc) {
			updateUserLocation("" + loc.getLatitude(), "" + loc.getLongitude());
			// boolean statusOfGPS = locationMangaer
			// .isProviderEnabled(LocationManager.GPS_PROVIDER);

			// Toast.makeText(
			// getBaseContext(),
			// "Lat: " + loc.getLatitude() + " Lng: " + loc.getLongitude(),
			// Toast.LENGTH_LONG).show();
			Log.e("Updates Location", "Loacation: " + time);
			// Toast.makeText(getBaseContext(),"Accuracy:"+loc.getAccuracy(),Toast.LENGTH_LONG).show();

		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
		}

		public void updateUserLocation(String lat, String lng) {

			long dtMili = System.currentTimeMillis();

			RequestParams params = new RequestParams();
			params.put(Constants.kapi_key,
					UserInfo.getInstance(ServiceUberGuest.this).api_key);
			params.put("time_stamp", String.valueOf(dtMili));
			params.put("latitude", lat);
			params.put("longitude", lng);
			params.put("device_id", ime);

			// Log.e("Request", Constants.api_base_url
			// + Constants.guest_update_location + params.toString());
			// send post request for user login
			AsyncHttpClient client = new AsyncHttpClient();
			client.get(
					Constants.api_base_url + Constants.guest_update_location,
					params, new JsonHttpResponseHandler() {

						@Override
						public void onSuccess(int status, JSONObject object) {
							// TODO Auto-generated method stub
							try {
								// Log.e("Responce", "hello");
								int stat = object.getInt("status");
								if (stat == 1) {
									if (object.has("property_name")) {
										String pname = object
												.getString("property_name");
										String plat = object
												.getString("latitude");
										String plong = object
												.getString("longitude");
										SharedPreferences sp = getSharedPreferences(
												"isNotified",
												Activity.MODE_PRIVATE);
										Log.e("MapBefore",
												"" + sp.getInt("notified", 0));
										sendNotification(pname, plat, plong);
									} else {
										Log.e("CallingSuccess",
												"yes no property");
										SharedPreferences sp = getSharedPreferences(
												"isNotified",
												Activity.MODE_PRIVATE);
										SharedPreferences.Editor editor = sp
												.edit();
										editor.putInt("notified", 0);
										editor.commit();
										IsNotificationSent = false;
									}
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

						@Override
						protected Object parseResponse(String arg0)
								throws JSONException {
							// TODO Auto-generated method stub
							Log.e("Responce", arg0);
							return super.parseResponse(arg0);
						}

						@SuppressLint("InlinedApi")
						@Override
						public void onFailure(Throwable throwable,
								JSONObject jsonObject) {
							int status = 0;
							try {
								status = jsonObject.getInt("status");
								Log.e("DeviceStatusFailure", "" + status);
								if (status == -2) {
									Intent home_intent = new Intent(
											ServiceUberGuest.this,
											LoginActivity.class);
									home_intent
											.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
									startActivity(home_intent);
									UserInfo userInfo = UserInfo
											.getInstance(ServiceUberGuest.this);
									userInfo.removeUserInfo(ServiceUberGuest.this);
									SharedPreferences sp = getSharedPreferences(
											"isNotified", Activity.MODE_PRIVATE);
									SharedPreferences.Editor editor = sp.edit();
									editor.putInt("notified", 0);
									editor.commit();
									IsNotificationSent = false;
									stopSelf();
								}
//								if (status == 1) {
//									Log.e("CallingFailure",
//											"yes no property sent noti false");
//									SharedPreferences sp = getSharedPreferences(
//											"isNotified", Activity.MODE_PRIVATE);
//									SharedPreferences.Editor editor = sp.edit();
//									editor.putInt("notified", 0);
//									editor.commit();
//									notificationManager.cancelAll();
//									IsNotificationSent = false;
//
//								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
//							super.onFailure(throwable, jsonObject);
						}
					});
		}

		@SuppressLint("NewApi")
		protected void sendNotification(String name, String lat, String lon) {
			// TODO Auto-generated method stub
			SharedPreferences sp = getSharedPreferences("isNotified",
					Activity.MODE_PRIVATE);
			int notified = sp.getInt("notified", 0);
			Log.e("inBackground", "" + sp.getInt("inFocus", 2));
			int inFocus = sp.getInt("inFocus", 2);
			if (notified == 0 && !IsNotificationSent) {
				Log.e("Notify", "yes");
				if (inFocus == 0) {
					Intent i = new Intent(getApplicationContext(),
							MapActivity.class);
					i.putExtra("property_name", name);
					i.putExtra("longitude", lon);
					i.putExtra("latitude", lat);
					// i.putExtra("fromNotification", "2");
					i.putExtra("notifyClicked", "3");

					PendingIntent pIntent = PendingIntent.getActivity(
							getApplicationContext(), 0, i, 0);
					// n = new Notification.Builder(getApplicationContext())
					// .setContentTitle("You Have Reached near " + name)
					// .setContentText("Uberguest")
					// .setSmallIcon(R.drawable.notificatio_icon)
					// .setContentIntent(pIntent)
					// .setAutoCancel(true)
					// .addAction(R.drawable.notificatio_icon,
					// "Check Property", pIntent).build();
//					n = new Notification.Builder(getApplicationContext())
//							.setContentTitle("You Have Reached near " + name)
//							.setContentText("Uberguest")
//							.setSmallIcon(R.drawable.notificatio_icon)
//							.setContentIntent(pIntent).setAutoCancel(true)
//							.build();
					n=new NotificationCompat.Builder(getApplicationContext()).setContentTitle("You Have Reached near " + name)
							.setContentText("Uberguest")
							.setSmallIcon(R.drawable.notificatio_icon)
							.setContentIntent(pIntent)
							.setAutoCancel(true)
							.build();
					// n.setLatestEventInfo(getApplicationContext(),
					// "You Have Reached near " + name, "Uberguest", pIntent);
					n.defaults |= Notification.DEFAULT_SOUND;
					n.defaults |= Notification.DEFAULT_VIBRATE;
					notificationManager.notify(0, n);
					IsNotificationSent = true;
				}

				Intent intent = new Intent("android.intent.action.MAIN")
						.putExtra("property_name", name)
						.putExtra("longitude", "" + lon)
						.putExtra("latitude", "" + lat);
				getApplicationContext().sendBroadcast(intent);
				SharedPreferences.Editor editor = sp.edit();
				editor.putInt("notified", 1);
				editor.putString("propertyName", name);
				editor.putString("cur_prop_long", lon);
				editor.putString("cur_prop_lat", lat);
				editor.commit();
				IsNotificationSent = true;
			}
			// else if(notified==1){
			// SharedPreferences.Editor editor = sp.edit();
			// editor.putInt("notified", 1);
			// editor.commit();
			// }

		}

	}

}
