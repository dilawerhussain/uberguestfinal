package com.tbox.uberguest;

import java.io.File;

import android.net.Uri;

public class Constants {
	
	// api request/response constants
	static public String request_post = "POST";
	static public String request_get = "GET";
	
	static public String api_base_url = "http://www.uberguest.com/uberguest_test/index.php/api/";
//	static public String api_base_url = "http://www.uberguest.com/uberguest_dev/index.php/api/";
	static public String guest_logout = "guest_logout";
	static public String response = "responce";
	static public String status = "status";
	static public String content_type_urlencoded = "application/x-www-form-urlencoded";
	static public String is_user_exists = "is_user_exists";
	static public String validate_security_answer = "validate_security_question";
	static public String guest_login = "guest_login";
	static public String guest_register = "register_guest";
	static public String guest_update_location = "guest_update_location";
	static public String guest_image_upload = "upload_guest_image/";
	static public String guest_voice_upload = "upload_guest_voice/";
	static public String guest_edit_profile = "edit_guest";
	static public String guest_search_property = "guest_search_property";
	
	static public int time ;
	
	//Api requst keys
	static public String kfirst_name = "first_name";
	static public String klast_name= "last_name";
	static public String kemail_id = "email_id";
	static public String ksecurity_answer = "s_answer";
	static public String kis_passwordchanged = "isPasswordChanged";
	static public String klatitude = "latitude";
	static public String klongitude = "longitude";
	static public String k_answer = "s_answer";
	static public String kapi_key = "api_key";
	static public String kcity = "city";
	static public String kcountry = "country";
	static public String ktitle = "title";
	static public String kcompany="company";
	static public String kreason = "reason_for_visit";
	static public String klike = "likes";
	static public String kdislike = "dislikes";
	
	static public String kstate = "state";
	
	static public String kspouse = "spouse";
	static public String kbirthday = "birthday";
	static public String kspecial_instruction = "special_instructions";
	static public String kcell_number = "cell_number";
	static public String kprofile_image_path = "picture";//to do, change the key to be profile_image_path
	static public String kvoice = "voice";//to do, change the key to be user_name_recording_path
	static public String kprofile_image = "profile_image";
	
	
	
	
	
	
	//////////helping static variables created by assad 
	static public int temp_image_gallery=0;
	static public Uri selected_image;
	static public File image_name;
	static public String picturePath;

}
