package com.tbox.backend;

import android.view.View;

public interface FileUploaderInterface {
	public void fileUploaded(FileUploader fileUploader);
	public float uploadProgress(FileUploader fileUploader,int downloadProgress);
}
